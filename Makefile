 .PHONY: envs request_data dna-explore dna-interim interfaces-explore interfaces-dataset autoencoder-train autoencoder-predict autoencoder-visualize fingerprints classifiers-dataset classifiers-train classifiers-visualize regression-train lint clean

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PROFILE = default
PROJECT_NAME = tf-dna-modelling


ifeq (,$(shell which conda 2> /dev/null))
HAS_CONDA=False
else
HAS_CONDA=True
PKG_MANAGER=conda
endif

ifeq (, $(shell which mamba 2> /dev/null))
HAS_MAMBA=False
else
HAS_MAMBA=True
PKG_MANAGER=mamba
endif

ifeq (, $(shell which micromamba 2> /dev/null))
HAS_MICROMAMBA=False
else
HAS_MICROMAMBA=True
PKG_MANAGER=micromamba
endif


#################################################################################
# COMMANDS                                                                      #
#################################################################################


## Set up mamba environments and install dependencies, for every step
envs:
	@echo 'Detected package manager: $(PKG_MANAGER)'
	
	$(PKG_MANAGER) env create -f envs/test_lint.yml  \
	-n $(PROJECT_NAME)-test_lint -y
	
	$(PKG_MANAGER) env create -f envs/data.yml  \
	-n $(PROJECT_NAME)-data -y
	
	$(PKG_MANAGER) env create -f envs/autoencoder-2024-10-17.yml  \
	-n $(PROJECT_NAME)-autoencoder -y
	
	$(PKG_MANAGER) env create -f envs/classifiers-regressors.yml  \
	-n $(PROJECT_NAME)-classifiers-regressors -y


## Request data from database
request_data:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-data  \
	python3 src/data/request_data.py  \
	--db_filepath ../tf-dna-sqlite/db/tf-dna-sqlite.db  \
	--source_dir ../tf-dna-sqlite/data/structures/  \
	--assembly_dir data/raw/assemblies/  \
	--interface_dir data/raw/interfaces/  \
	--interacting_dataset_filepath data/raw/interacting_dataset.csv  \
	> logs/request_data.out 2> logs/request_data.err


## Start building dataset of bound DNA sequences
dna-interim:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-data  \
	python3 src/dna_pipeline/0_interim.py  \
	--assembly_dir data/raw/assemblies/  \
	--interacting_dataset_filepath data/raw/interacting_dataset.csv  \
	--output_filepath data/interim/dna.csv  \
	> logs/dna-interim.out 2> logs/dna-interim.err


## Explore data from bound DNA sequences, plots and statistics
dna-explore:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-data  \
	python3 src/dna_pipeline/1_explore.py  \
	--input_filepath data/interim/dna.csv  \
	--figures_dir reports/figures/dna-explore  \
	> logs/dna-explore.out 2> logs/dna-explore.err


## Filtering and processing of bound DNA sequences
dna-filter:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-data  \
	python3 src/dna_pipeline/2_filter.py  \
	--input_filepath data/interim/dna.csv  \
	--output_filepath data/interim/dna-filtered.csv  \
	> logs/dna-filter.out 2> logs/dna-filter.err


## Start building dataset of bound DNA sequences
dna-generate-counterexamples:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-data  \
	python3 src/dna_pipeline/3_counterexamples.py  \
	--input_filepath data/interim/dna-filtered.csv  \
	--output_filepath data/interim/dna-counterexamples.csv  \
	> logs/dna-counterexamples.out 2> logs/dna-counterexamples.err


## Encode DNA sequences, using one-hot encoding
dna-encode-1he:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-data  \
	python3 src/dna_pipeline/4_encode_1he.py  \
	--real_dataset_filepath data/interim/dna-filtered.csv  \
	--counterexample_dataset_filepath data/interim/dna-counterexamples.csv  \
	--output_filepath data/processed/dna-encode-1he.csv  \
	> logs/dna-encode-1he.out 2> logs/dna-encode-1he.err


## Encode DNA sequences, using three-body matrix
dna-encode-3bm:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-classifiers-regressors  \
	python3 src/dna_pipeline/4_encode_3bm.py  \
	--real_dataset_filepath data/interim/dna-filtered.csv  \
	--counterexample_dataset_filepath data/interim/dna-counterexamples.csv  \
	--output_filepath data/processed/dna-encode-3bm.csv  \
	> logs/dna-encode-3bm.out 2> logs/dna-encode-3bm.err


## Explore data from interfaces, plots and statistics
interfaces-explore:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-data  \
	python3 src/interface_pipeline/0_explore.py  \
	--interface_dir data/raw/interfaces/  \
	--figures_dir reports/figures/interfaces-explore  \
	> logs/interfaces-explore.out 2> logs/interfaces-explore.err


## Make dataset file from interfaces
interfaces-dataset:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-data  \
	python3 src/interface_pipeline/1_dataset.py  \
	--interface_dir data/raw/interfaces/  \
	--output_filepath data/processed/dataset_autoencoder.pkl  \
	> logs/interfaces-dataset.out 2> logs/interfaces-dataset.err


## Train autoencoder neural network on dataset of interfaces
autoencoder-train: dataset_autoencoder.pkl
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-autoencoder  \
	python3 src/interface_pipeline/2_train.py  \
	--dataset_filepath data/processed/dataset_autoencoder.pkl  \
	> logs/autoencoder-train.out 2> logs/autoencoder-train.err


## Predict contact maps from fine-tuned autoencoder neural network
autoencoder-predict:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-autoencoder  \
	python3 src/interface_pipeline/3_predict.py
	--dataset_filepath data/processed/dataset_autoencoder.pkl  \
	--model_dir models/autoencoder_90/  \
	--output_filepath data/processed/cmaps_autoencoder.pkl  \
	> logs/autoencoder-predict.out 2> logs/autoencoder-predict.err


## Visualize autoencoder losses
autoencoder-visualize:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-classifiers-regressors  \
	python3 src/interface_pipeline/4_visualize.py  \
	--autoencoder_dir models/autoencoder_90  \
	--figures_dir reports/figures/autoencoder  \
	> logs/autoencoder-visualize.out 2> logs/autoencoder-visualize.err


## Extract molecular fingerprints from fine-tuned autoencoder neural network
fingerprints:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-autoencoder  \
	python3 src/interface_pipeline/5_fingerprints.py  \
	--dataset_filepath data/processed/cmaps_autoencoder.pkl  \
	--model_dir models/autoencoder_90/  \
	--output_filepath data/processed/fingerprints_autoencoder.pkl  \
	> logs/fingerprints.out 2> logs/fingerprints.err


## Make dataset file from both encoded DNA sequences and interface fingeerprints
classifiers-dataset:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-data  \
	python3 src/classification/0_dataset.py  \
	--dna_dataset_filepath data/processed/dna-encode-3bm.csv  \
	--fingerprints_dataset_filepath data/processed/fingerprints_autoencoder.pkl  \
	--output_filepath data/processed/dataset_classifiers.csv  \
	> logs/classifiers-dataset.out 2> logs/classifiers-dataset.err


## Train several classifers
classifiers-train:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-classifiers-regressors  \
	python3 src/classification/1_train.py  \
	--dataset_filepath data/processed/dataset_classifiers.csv  \
	--classifiers_dir models/classifiers  \
	> logs/classifiers-train.out 2> logs/classifiers-train.err


## Visualize summaries of several trained classifers
classifiers-visualize:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-classifiers-regressors  \
	python3 src/classification/2_visualize.py  \
	--dataset_filepath data/processed/dataset_classifiers.csv  \
	--classifiers_dir models/classifiers  \
	--figures_dir reports/figures/classification  \
	> logs/classifiers-visualize.out 2> logs/classifiers-visualize.err


## Build dataset file from DNA sequences
regression-dataset:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-classifiers-regressors  \
	python3 src/regression/0_dataset.py  \
	--dna_dataset_filepath data/processed/dna-encode-3bm.csv  \
	--fingerprints_dataset_filepath data/processed/fingerprints_autoencoder.pkl  \
	--output_filepath data/processed/dataset_regressors.csv  \
	> logs/regression-dataset.out 2> logs/regression-dataset.err


## Train several regressors
regression-train:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-classifiers-regressors  \
	python3 src/regression/1_train.py  \
	--dataset_filepath data/processed/dataset_regressors.csv  \
	--regressors_dir models/regressors  \
	> logs/regression-train.out 2> logs/regression-train.err


## Visualize summaries of several trained regressors
regression-visualize:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-classifiers-regressors  \
	python3 src/regression/2_visualize.py  \
	--dataset_filepath data/processed/dataset_regressors.csv  \
	--regressors_dir models/regressors  \
	--figures_dir reports/figures/regression  \
	> logs/regression-visualize.out 2> logs/regression-visualize.err


## Linting
lint:
	$(PKG_MANAGER) run -n $(PROJECT_NAME)-test_lint flake8 src/


## Delete all compiled Python files
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete


#################################################################################
# PROJECT RULES                                                                 #
#################################################################################



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
