"""Make datasets from PDB files."""

import sys
import logging
from pathlib import Path
import click

import re

# from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.MMCIFParser import MMCIFParser
import numpy as np
import pandas as pd
import pickle


def get_positions_from_file(interface_file):
    """Get spatial positions and element of all atoms from a structure."""
    pdb_id = Path(interface_file).stem[:4]

    parser = MMCIFParser(QUIET=True)
    structure = parser.get_structure(pdb_id, interface_file)

    data = []

    for atom in structure.get_atoms():
        x, y, z = atom.coord
        element = atom.element

        # for debugging, just to catch weird atoms
        # if len(element) > 1 or element == "U" or element == "X"::
        #     print(pdb_id, element)

        data.append((element, x, y, z))

    # cast into dataframe, naming rows with atom names
    df = pd.DataFrame(data, columns=["element", "x", "y", "z"])
    logger.info(f"Coordinates from {interface_file} of shape {df.shape}")

    return df


def get_positions_from_dir(interface_dir):
    """Get spatial positions of all atoms from all structures."""
    interface_dir = Path(interface_dir)

    pos_list = []  # list of Dataframe, one for each structure
    interface_id_list = []  # list of interfaces id for each structure

    for filepath in interface_dir.iterdir():
        # check that it is a PDB file
        if (not filepath.is_file()) or filepath.suffix not in [".cif", ".pdb"]:
            logger.info(f"Skipped {filepath.as_posix()}")
            continue  # to the next file

        # single interface: coordinates and elements in a dataframe
        # logger.info(f"Getting coordinates from {filepath.as_posix()}")
        df = get_positions_from_file(filepath.as_posix())
        pos_list.append(df)
        interface_id = re.search(
            pattern="(....:.+:.+:[0-9]+)-interface", string=filepath.name
        )[1]
        interface_id_list.append(interface_id)

    # Combine interface ids with their respective coordinates
    data = dict(zip(interface_id_list, pos_list))
    logger.info(f"{len(data)} dataframes of coordinates and elements.")
    return data


def map_element_set(element_set):
    """Map original element set to a restricted element set."""
    element_set_mapped = set()
    element_map = {}  # Map original elements to restricted elements

    for elem in element_set:
        if elem in ["ER", "HG", "MG", "SE", "ZN", "CA", "CD", "CL", "NA", "X"]:
            element_set_mapped.add("ION")
            element_map[elem] = "ION"
        else:
            element_set_mapped.add(elem)
            element_map[elem] = elem
    return list(element_set_mapped), element_map


@click.command()
@click.option(
    "--interface_dir",
    type=click.Path(exists=True),
    help="Directory with all the interfaces to be used as dataset.",
)
@click.option(
    "--output_filepath",
    type=click.Path(),
    help="Output filepath, dataset dump.",
)
def main(interface_dir, output_filepath):
    """Make and dump dataset of atom coordinates from interfaces."""
    # dictionary with interface ids and dataframes with xyz coordinates
    data = get_positions_from_dir(interface_dir)

    # set of unique atoms
    element_set = []
    for pos in data.values():
        element_set += pos["element"].tolist()
    element_set = list(set(element_set))
    element_set.sort()

    # map the set of elements into a smaller set
    element_set_mapped, element_map = map_element_set(element_set)

    # Create dummies for the mapped element set
    dummies_mapped = pd.get_dummies(element_set_mapped).to_numpy()

    # Combine into a dictionary, by mapping the "mapped" elements
    # with the corresponding "mapped" dummies
    # basically: naming the rows of the matrix
    dummies_mapped = dict(zip(element_set_mapped, dummies_mapped))

    # Combine into a dictionary, by mapping the "original" elements
    # to the corresponding "mapped" dummies
    # basically: naming the rows of the matrix
    dummies = {elem: dummies_mapped[element_map[elem]] for elem in element_set}

    # all data objects, one for each structure
    dataset = []

    for interface_id, pos in data.items():
        # initialize 2d matrix
        # shape is (n atoms of a structure, n unique atoms in total)
        x = np.zeros((len(pos), len(element_set_mapped)))

        # pop element array, to be added in final dictionary
        elements = pos["element"].tolist()

        for i, elem in enumerate(pos["element"]):
            # fill 2d matrix with one-hot-encoding row
            x[i] = dummies[elem]

            # map original elements to mapped elements
            # i.e. C -> C, ZN -> ION, etc
            elements[i] = element_map[elem]

        # cast from pandas dataframe into numpy matrix
        # by removing the element column, we are casting only the coordinates
        pos = pos.drop("element", axis=1).to_numpy()

        # dictionary ready to be casted into a Data object in the next step
        # only x and pos are used in the Data Object
        # interface_id is used to keep track which is which in the cmaps
        # elements are used in the fingerprints encoding
        dataset.append({
            "interface_id": interface_id,
            "elements": elements,
            "x": x,
            "pos": pos,
        })

    # dump dataset
    logger.info(f"Dumping dataset into {output_filepath}")
    with open(output_filepath, "wb") as f:
        pickle.dump(dataset, f)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
