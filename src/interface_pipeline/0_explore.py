"""Explore proteic interfaces in mmCif files."""

import sys
import logging
from pathlib import Path
import click

import re

# from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.MMCIFParser import MMCIFParser
import pandas as pd
from scipy.spatial.distance import pdist
import matplotlib.pyplot as plt
import seaborn as sns


def get_positions_from_file(interface_file):
    """Get spatial positions and element of all atoms from a structure."""
    pdb_id = Path(interface_file).stem[:4]

    parser = MMCIFParser(QUIET=True)
    structure = parser.get_structure(pdb_id, interface_file)

    data = []

    for atom in structure.get_atoms():
        x, y, z = atom.coord
        element = atom.element

        data.append((element, x, y, z))

    # cast into dataframe, naming rows with atom names
    df = pd.DataFrame(data, columns=["element", "x", "y", "z"])
    logger.info(f"Coordinates from {interface_file} of shape {df.shape}")

    return df


def get_positions_from_dir(interface_dir):
    """Get spatial positions of all atoms from all structures."""
    interface_dir = Path(interface_dir)

    pos_list = []  # list of Dataframe, one for each structure
    interface_id_list = []  # list of interfaces id for each structure

    for filepath in interface_dir.iterdir():
        # check that it is a PDB file
        if (not filepath.is_file()) or filepath.suffix not in [".cif", ".pdb"]:
            logger.info(f"Skipped {filepath.as_posix()}")
            continue  # to the next file

        # single interface: coordinates and elements in a dataframe
        # logger.info(f"Getting coordinates from {filepath.as_posix()}")
        df = get_positions_from_file(filepath.as_posix())
        pos_list.append(df)
        interface_id = re.search(
            pattern="(....:.+:.+:[0-9]+)-interface", string=filepath.name
        )[1]
        interface_id_list.append(interface_id)

    # Combine interface ids with their respective coordinates
    data = dict(zip(interface_id_list, pos_list))
    logger.info(f"{len(data)} dataframes of coordinates and elements.")
    return data


@click.command()
@click.option(
    "--interface_dir",
    type=click.Path(exists=True),
    help="Directory with all the interfaces to be used as dataset.",
)
@click.option(
    "--figures_dir",
    type=click.Path(),
    help="Output filepath, dump of exploration object.",
)
def main(interface_dir, figures_dir):
    """Explore dataset of atom coordinates from interfaces."""
    # dictionary with interface ids and dataframes with xyz coordinates
    data = get_positions_from_dir(interface_dir)

    # list of all elements, not unique, to be counted
    element_list = []
    for pos in data.values():
        element_list += pos["element"].tolist()

    sns.set_theme(style='ticks')
    # Define the palette as a list to specify exact values
    # palette = sns.color_palette("viridis")

    g = sns.catplot(
        data=element_list,
        kind="count",
        height=5,
        aspect=1,
        # palette=palette,
    )
    g.set(xlabel="Count", ylabel="Atomic Element")
    g.fig.subplots_adjust(top=0.9)
    g.fig.subplots_adjust(left=0.12)
    # g.fig.suptitle(
    #     "Absolute Frequencies of Atomic Elements present in Proteic \
    # Interfaces"
    # )
    out_filepath = f"{figures_dir}/elements-count.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(f"Count plot of Atomic Elements, saved at {out_filepath}")

    g = sns.catplot(
        data=element_list,
        kind="count",
        height=5,
        aspect=1,
        # palette=palette,
    )
    g.set(xlabel="Count", ylabel="Atomic Element")
    g.set(xscale="log")
    g.fig.subplots_adjust(top=0.9)
    g.fig.subplots_adjust(left=0.12)
    # g.fig.suptitle(
    #     "Absolute Frequencies of Atomic Elements present in Proteic \
    # Interfaces"
    # )
    out_filepath = f"{figures_dir}/elements-logcount.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(f"Count plot of Atomic Elements, saved at {out_filepath}")

    # atom distances (in Armstrong)
    distances = []
    for pos in data.values():
        # pos is dataframe
        # Returns a condensed distance matrix
        dist = pdist(pos[['x', 'y', 'z']].values)
        # Converts to a square distance matrix
        distances += list(dist)

    plt.figure(figsize=(6, 6))
    ax = sns.histplot(distances, log=False, kde=True)
    # ax.set_title("Distribution of Atomic Distances in Proteic Interfaces")
    ax.set_xlabel("Atomic Distance (in Armstrong)")
    out_filepath = f"{figures_dir}/distances-hist.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(f"Histogram of Atomic Distances, saved at {out_filepath}")

    plt.figure(figsize=(6, 6))
    ax = sns.histplot(distances, log=True, kde=True)
    # ax.set_title("Distribution of Atomic Distances in Proteic Interfaces")
    ax.set_xlabel("Atomic Distance (in Armstrong)")
    out_filepath = f"{figures_dir}/distances-loghist.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(f"Histogram of Atomic Distances, saved at {out_filepath}")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
