"""Making fingerprints using trained autoencoder model."""

import sys
import logging
from pathlib import Path
import click

import pickle
from operator import itemgetter
import numpy as np

import torch
from e3nn.nn.models.v2103.gate_points_networks import SimpleNetwork

# local imports
from autoencoder import Autoencoder_cmap
from utils import load_hyperparams


def build_fingerprints(
    latent_dataset, element_set, bins, output_dir="data/processed/"
):
    """Build fingerprints, given bins number."""
    bins_fingerprints = {}

    # Iteration over all unique elements
    for element in element_set:
        logger.info(f"Binning latent values of atomic element {element}")

        # i.e. latent values of the oxygen across all interfaces
        element_latent_values = []

        # iteration over each data inside the original dataset
        for d in latent_dataset:
            logger.info(f"Elements from interface: {d['interface_id']}")

            # i.e. latent values of the oxygen from interface 3UK3:3:C:30
            interface_latent_values = [
                d["latent_vec"][i]  # i-th latent value
                for i, elem in enumerate(d["elements"])
                if elem == element  # corresponding to i-th element
                # if it's the element we're currently counting
            ]

            # extend list of latent values
            element_latent_values += interface_latent_values

        # all the latent values
        element_latent_values = np.array(element_latent_values)
        element_latent_values = np.sort(element_latent_values)

        # Here selection for upper and lower values which include
        # 90% of all data
        # to "discard" outlayers, otherwise outlayers make the range too big
        lower_index = int(0.05 * len(element_latent_values))
        upper_index = int(0.95 * len(element_latent_values))

        # Build the histogram using the upper and lower values
        # and the number of bins specified by user
        _, bin_edges = np.histogram(
            element_latent_values,
            bins=bins,
            range=(
                element_latent_values[lower_index],
                element_latent_values[upper_index],
            ),
        )

        # Save the values of each bins into a dictionary
        # where the key is the atom type
        bins_fingerprints[element] = bin_edges

    # initialize the dataset for the fingerprints, to be used as output
    fingerprint_list = []

    for d in latent_dataset:
        logger.info(f"Fingerprint from interface: {d['interface_id']}")

        # initialize the fingerprint (matrix)
        # Here each histo has num_bins+2 bins. The "+2" is because
        # if a latent value of a atom fall outside the histo, it'ss placed in
        # one of those two additional bins
        # depending if it falls on the right or left of histogram
        fingerprint = np.zeros(shape=(len(element_set), bins + 2))

        # iteration over atom in the data
        for elem, latent_value in zip(d["elements"], d["latent_vec"]):

            # detect (with numpy digitize) the index of the histogram a latent
            # value of an atom belongs to
            # The histo is selected using the dictionary bins_fingerprints
            # which connects each atom type with its histo
            index = int(np.digitize(latent_value, bins_fingerprints[elem]))

            # Given the index of the histo, the fingerprint is incremented
            # by one in the position of the index
            # at proper row(?). The row(?) is determined by the atom type.
            # The order of the rows(?) is kept along all the fingerprints
            # thanks to the usage of element_set.index(elem)
            # which detects the row(?) value
            # using the pre-specified order in all_atom_types_list
            fingerprint[element_set.index(elem), index] += 1

        # save all info of that data into the list
        fingerprint_list.append((d["interface_id"], fingerprint))

    return fingerprint_list


@click.command()
@click.option(
    "--dataset_filepath",
    type=click.Path(exists=True),
    help="Filepath to dataset dump.",
)
@click.option(
    "--model_dir",
    type=click.Path(exists=True),
    help="Filepath to model directory.",
)
@click.option(
    "--output_filepath",
    type=click.Path(),
    help="Filepath to predictions output.",
)
def main(dataset_filepath, model_dir, output_filepath):
    """Make fingerprints from predictions of the encoder."""
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    logger.info(f"Device where I am running: {device}")

    # get hyper-parameters from fine-tuned model directory
    hyperparams = load_hyperparams(model_dir)
    (
        max_radius_e3nn,
        n_layers_e3nn,
        lmax_e3nn,
        mul_e3nn,
        n_elements,
        n_atoms_per_structure,
        batch_size,
        activ_func_mlp_cmap,
        n_nodes_mlp_cmap,
        n_layers_mlp_cmap,
        n_epochs,
        learning_rate,
    ) = itemgetter(
        "max_radius_e3nn",
        "n_layers_e3nn",
        "lmax_e3nn",
        "mul_e3nn",
        "n_elements",
        "n_atoms_per_structure",
        "batch_size",
        "activ_func_mlp_cmap",
        "n_nodes_mlp_cmap",
        "n_layers_mlp_cmap",
        "n_epochs",
        "learning_rate",
    )(
        hyperparams
    )

    logger.info("Load data from original dump dataset.")
    with open(dataset_filepath, "rb") as f:
        dict_dataset = pickle.load(f)

    # neural network models
    encoder = SimpleNetwork(
        irreps_in=f"{n_elements}x0e",  # based on n_elements, ex: "6x0e"
        irreps_out="1x0e",
        max_radius=max_radius_e3nn,
        num_neighbors=15,
        num_nodes=n_atoms_per_structure,
        layers=n_layers_e3nn,
        lmax=lmax_e3nn,
        mul=mul_e3nn,
        pool_nodes=False,
    ).to(device)
    autoencoder = Autoencoder_cmap(
        encoder,  # simple network from above
        n_atoms_per_structure,
        n_elements,
        batch_size,
        activ_func_mlp_cmap,
        n_nodes_mlp_cmap,
        n_layers_mlp_cmap,
        device,
    ).to(device)
    logger.info("Set up autoencoder model.")

    # Carica i pesi addestrati
    model_state_filepath = f"{model_dir}/model_train.pt"
    autoencoder.load_state_dict(
        torch.load(model_state_filepath, map_location=device)
    )
    logger.info("Loaded weigths for autoencoder model.")

    # Imposta in modalità di valutazione
    autoencoder.eval()

    # we define here a temporary dataset where to save all the latent vectors
    latent_dataset = []

    # Disabilita i gradienti per risparmiare memoria
    with torch.no_grad():
        for d in dict_dataset:
            # get coordinates positions for each data in orginal dataset
            pos = d["pos"]
            pos = torch.from_numpy(pos)
            pos = pos.to(torch.float32)
            pos = pos.to(device)

            # get atom type for each data in orignale dataset
            x = d["x"]
            x = torch.from_numpy(x)
            x = x.to(torch.float32)
            x = x.to(device)

            # get interface id
            interface_id = d["interface_id"]
            logger.info(f"Latent vector from interface {interface_id}")

            # get the latent vector for each data using the encoder sub-model
            # in the trained autoencoder
            # autoencoder.SN_cmap is the same of encoder
            # but in autoencoder.SN_cmap we loaded the
            # parameters from the trained model
            out = autoencoder.SN_cmap({"pos": pos, "x": x}).reshape(-1)

            # output latent vector to CPU and then casted into numpy
            # column vector of shape (30,)
            # where 30 is the number of atoms per interface
            out = out.cpu().numpy()

            # Save in dataset_latent all the info from the original dataste
            # but now including the latent vector
            latent_dataset.append(
                {
                    "interface_id": d["interface_id"],
                    "elements": d["elements"],
                    "x": d["x"],
                    "pos": d["pos"],
                    "latent_vec": out,
                    "original_cmap": d["original_cmap"],
                    "pred_cmap": d["pred_cmap"],
                    "fingerprints": {},  # dictionary for all fingerprints
                }
            )

    element_set = []
    for data in latent_dataset:
        element_set += data["elements"]
    element_set = list(set(element_set))
    element_set.sort()
    logger.info(f"Set of all atoms in the dataset: {element_set}")

    # TODO as argument
    bins_list = [2, 4, 5, 6, 8, 10, 12, 14]

    for bins in bins_list:
        logger.info(f"Build fingerprints fingerprints for {bins} bins.")
        fingerprint_list = build_fingerprints(
            latent_dataset, element_set, bins
        )
        for d, tupla in zip(latent_dataset, fingerprint_list):
            interface_id, fingerprint = tupla
            assert interface_id == d["interface_id"]
            # add to fingerprint dictionary
            # key=bins : value=fingerprint
            d["fingerprints"][bins] = fingerprint

    # export output as a dump file
    with open(output_filepath, "wb") as f:
        pickle.dump(latent_dataset, f)
    logger.info(
        f"Interface data with latent vector and fingerprints saved in \
{output_filepath}"
    )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
