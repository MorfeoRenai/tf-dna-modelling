"""Predicting contact maps using trained autoencoder model."""

import sys
import logging
from pathlib import Path
import click

import pickle
from operator import itemgetter

import torch
from torch_geometric.data import Data
from torch_geometric.loader import DataLoader
from e3nn.nn.models.v2103.gate_points_networks import SimpleNetwork

# local imports
from autoencoder import Autoencoder_cmap
from utils import load_hyperparams


def build_data_loader(dataset_filepath, device):
    """
    Build single DataLoader from a dump of dictionaries with data.

    Dump contains dictionaries with keys: "x", "pos", "interface_id".
    """
    with open(dataset_filepath, "rb") as f:
        dict_dataset = pickle.load(f)

    dataset = []
    for d in dict_dataset:
        # pos are positions in space
        # matrix of shape (n_atoms_per_structure, 3)
        pos = d["pos"]
        pos = torch.from_numpy(pos)
        pos = pos.to(torch.float32)
        pos = pos.to(device)

        # x are dummies
        # matrix of shape (n_atoms_per_structure, n_elements)
        x = d["x"]
        x = torch.from_numpy(x)
        x = x.to(torch.float32)
        x = x.to(device)

        # data object from torch geometric (pyg)
        data = Data(pos=pos, x=x)
        data.to(device)
        dataset.append(data)

    # instantiate DataLoader object
    # shuffle must be false, if not we can't ensure that the 1st interface id
    # correspond to the 1st batch (composed by 1 Data obj) and so on
    data_loader = DataLoader(dataset, batch_size=1, shuffle=False)

    return data_loader


@click.command()
@click.option(
    "--dataset_filepath",
    type=click.Path(exists=True),
    help="Filepath to dataset dump for autoencoder.",
)
@click.option(
    "--model_dir",
    type=click.Path(exists=True),
    help="Filepath to model directory.",
)
@click.option(
    "--output_filepath",
    type=click.Path(),
    help="Filepath to datset dump output containing contact maps.",
)
def main(dataset_filepath, model_dir, output_filepath):
    """Predictions from model: encoder-decoder."""
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    logger.info(f"Device where I am running: {device}")

    # get hyper-parameters from fine-tuned model directory
    hyperparams = load_hyperparams(model_dir)
    (
        max_radius_e3nn,
        n_layers_e3nn,
        lmax_e3nn,
        mul_e3nn,
        n_elements,
        n_atoms_per_structure,
        batch_size,
        activ_func_mlp_cmap,
        n_nodes_mlp_cmap,
        n_layers_mlp_cmap,
        n_epochs,
        learning_rate,
    ) = itemgetter(
        "max_radius_e3nn",
        "n_layers_e3nn",
        "lmax_e3nn",
        "mul_e3nn",
        "n_elements",
        "n_atoms_per_structure",
        "batch_size",
        "activ_func_mlp_cmap",
        "n_nodes_mlp_cmap",
        "n_layers_mlp_cmap",
        "n_epochs",
        "learning_rate",
    )(
        hyperparams
    )

    with open(dataset_filepath, "rb") as f:
        dict_dataset = pickle.load(f)
    logger.info("Loaded original dataset.")

    # load dataset and build dataloaders
    loader = build_data_loader(dataset_filepath, device)
    logger.info("Built data objects from original dataset.")

    # neural network models
    encoder = SimpleNetwork(
        irreps_in=f"{n_elements}x0e",  # based on n_elements, ex: "6x0e"
        irreps_out="1x0e",
        max_radius=max_radius_e3nn,
        num_neighbors=15,
        num_nodes=n_atoms_per_structure,
        layers=n_layers_e3nn,
        lmax=lmax_e3nn,
        mul=mul_e3nn,
        pool_nodes=False,
    ).to(device)
    autoencoder = Autoencoder_cmap(
        encoder,  # simple network from above
        n_atoms_per_structure,
        n_elements,
        batch_size,
        activ_func_mlp_cmap,
        n_nodes_mlp_cmap,
        n_layers_mlp_cmap,
        device,
    ).to(device)
    logger.info("Set up autoencoder model.")

    # Carica i pesi addestrati
    model_state_filepath = f"{model_dir}/model_train.pt"
    autoencoder.load_state_dict(
        torch.load(model_state_filepath, map_location=device)
    )
    logger.info("Loaded weigths for autoencoder model.")

    # Imposta in modalità di valutazione
    autoencoder.eval()

    cmap_data = []

    # Disabilita i gradienti per risparmiare memoria
    with torch.no_grad():
        for d, batch in zip(dict_dataset, loader):
            # check if original dataset and data loader follow the same order
            # if not, raise AssertionError
            assert (batch.x.cpu().numpy() == d["x"]).all()
            assert (batch.pos.cpu().numpy() == d["pos"]).all()
            interface_id = d["interface_id"]
            logger.info(f"Current interface: {interface_id}")

            # Sposta i dati sul dispositivo specificato
            # in teoria dovrebbero gia' essere su device
            batch.to(device)

            # atom distances of the structure in the batch
            # AKA contact map AKA cmap
            original_cmap = torch.cdist(
                # batch["pos"][batch["ptr"][0]: batch["ptr"][1]],
                # batch["pos"][batch["ptr"][0]: batch["ptr"][1]],
                # this is equal to batch["pos"] because bath len == 1
                batch["pos"], batch["pos"]
            ).to(device)

            # get predictions/outputs from model, using the forward pass
            # not yet a cmap, it's a tensor with shape
            # (batch_size, n_atoms_per_structure**2)
            out = autoencoder(batch, batch_size=1)

            # reshape predictions in a square matrix, now it's a cmap
            pred_cmap = out.reshape(
                n_atoms_per_structure, n_atoms_per_structure
            )

            # Porta su CPU e converti in numpy array
            original_cmap = original_cmap.cpu().numpy()
            pred_cmap = pred_cmap.cpu().numpy()

            # add to output
            cmap_data.append({
                "interface_id": interface_id,
                "elements": d["elements"],
                "x": d["x"],
                "pos": d["pos"],
                "original_cmap": original_cmap,
                "pred_cmap": pred_cmap,
                # TODO add distance metric between two contact maps
                # "distance": ...
            })

    # export output as a dump file
    with open(output_filepath, "wb") as f:
        pickle.dump(cmap_data, f)
    logger.info(f"Original and predicted contact maps saved in \
{output_filepath}")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
