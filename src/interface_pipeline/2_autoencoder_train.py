"""Training and validating autoencoder model."""

import sys
import logging
from pathlib import Path
import click

from tqdm import tqdm
import pickle
import numpy as np
import torch

# from torch.nn.functional import pad
from torch_geometric.data import Data
from torch_geometric.loader import DataLoader
from torch.utils.data import random_split
from torch.optim import Adam

# from torch.optim.lr_scheduler import ReduceLROnPlateau
from e3nn.nn.models.v2103.gate_points_networks import SimpleNetwork

# local imports
from autoencoder import Autoencoder_cmap
from utils import create_new_autoencoder_folder
from utils import validate_positive_int
from utils import save_hyperparams


def save_model(
    min_train_loss,
    min_val_loss,
    autoencoder,
    epoch_loss_train,
    epoch_loss_val,
    autoencoder_dir,
):
    """
    Early stopping function.

    Function to save the model in case the current epoch loss are smaller then
    previous min values.
    """
    if min_train_loss > epoch_loss_train:
        min_train_loss = epoch_loss_train
        torch.save(
            autoencoder.state_dict(), f"{autoencoder_dir}/model_train.pt"
        )

    if min_val_loss > epoch_loss_val:
        min_val_loss = epoch_loss_val
        torch.save(
            autoencoder.state_dict(), f"{autoencoder_dir}/model_train.pt"
        )

    return min_train_loss, min_val_loss


def build_data_loaders(dataset_filepath, batch_size, device):
    """
    Build train and test DataLoaders from a dump of dictionaries with data.

    Dump contains dictionaries with keys: "x", "pos", "interface_id".
    """
    with open(dataset_filepath, "rb") as f:
        dict_dataset = pickle.load(f)

    dataset = []
    for d in dict_dataset:
        # pos are positions in space
        # matrix of shape (n_atoms_per_structure, 3)
        pos = d["pos"]
        pos = torch.from_numpy(pos)
        pos = pos.to(torch.float32)
        pos = pos.to(device)

        # x are dummies
        # matrix of shape (n_atoms_per_structure, n_elements)
        x = d["x"]
        x = torch.from_numpy(x)
        x = x.to(torch.float32)
        x = x.to(device)

        # data object from torch geometric (pyg)
        data = Data(pos=pos, x=x)
        data.to(device)
        dataset.append(data)

    # split dataset into training and validation sets
    train_set, val_set = random_split(dataset, [0.8, 0.2])

    # instantiate DataLoader objects for each split
    train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=True)
    val_loader = DataLoader(val_set, batch_size=batch_size, shuffle=False)

    return (train_loader, val_loader)


# @click.option(
# not used in this script, used in padding
# if used, needs to be computed automatically
#     "--max_cmap_distance",
#     callback=validate_positive_int,
#     type=float,
#     help="Maximum distance in the contact map.",
# )
@click.command()
@click.option(
    "--dataset_filepath",
    type=click.Path(exists=True),
    help="Filepath to dataset dump.",
)
@click.option(
    "--batch_size",
    default=64,
    callback=validate_positive_int,
    type=int,
    help="Batch size.",
)
@click.option(
    "--n_epochs",
    default=300,
    callback=validate_positive_int,
    type=int,
    help="Number of epochs in the training process.",
)
@click.option(
    "--activ_func_mlp_cmap",
    default="ELU",
    type=click.Choice(["ELU", "Tanh"]),
    help="Activation function of the MLP contact map.",
)
@click.option(
    "--activ_func_mlp_atom",
    default="ELU",
    type=click.Choice(["ELU", "Tanh"]),
    help="Activation function of the atom MLP.",
)
@click.option(
    "--n_nodes_mlp_cmap",
    default=500,
    callback=validate_positive_int,
    type=int,
    help="Number of nodes in each layer of the contact map MLP.",
)
@click.option(
    "--n_nodes_mlp_atom",
    default=1,
    callback=validate_positive_int,
    type=int,
    help="Number of nodes in each layer of the atom MLP.",
)
@click.option(
    "--n_layers_mlp_cmap",
    default=3,
    callback=validate_positive_int,
    type=int,
    help="Number of layers in the contact map MLP.",
)
@click.option(
    "--n_layers_mlp_atom",
    default=1,
    callback=validate_positive_int,
    type=int,
    help="Number of layers in the atom MLP.",
)
@click.option(
    "--max_radius_e3nn",
    default=12.0,
    type=float,
    help="Cutoff value implemented by e3nn.",
)
@click.option(
    "--n_layers_e3nn",
    default=3,
    callback=validate_positive_int,
    type=int,
    help="Number of layers in the e3nn Graph neural network.",
)
@click.option(
    "--lmax_e3nn",
    default=1,
    callback=validate_positive_int,
    type=int,
    help="Maximum value for l implemented by e3nn.",
)
@click.option(
    "--mul_e3nn",
    default=8,
    callback=validate_positive_int,
    type=int,
    help="Value of mul implemented by e3nn.",
)
@click.option(
    "--learning_rate",
    default=1e-3,
    type=float,
    help="Learning rate for optimizer.",
)
@click.option(
    "--weight_decay",
    default=0.0,
    type=float,
    help="Weight decay for optimizer.",
)
@click.option(
    "--constant_losses",
    default=1.0,
    type=float,
    help="Proportional constant between losses.",
)
def main(
    dataset_filepath,
    batch_size,
    n_epochs,
    max_radius_e3nn,
    n_layers_e3nn,
    lmax_e3nn,
    mul_e3nn,
    activ_func_mlp_cmap,
    activ_func_mlp_atom,  # TODO not used?? ask riccardo
    n_nodes_mlp_cmap,
    n_nodes_mlp_atom,  # TODO not used?? ask riccardo
    n_layers_mlp_cmap,
    n_layers_mlp_atom,  # TODO not used?? ask riccardo
    weight_decay,
    constant_losses,
    learning_rate,
    # max_cmap_distance,  # not used in this script, used in padding
):
    """Training and validating model: encoder-decoder."""
    # TODO device agnostic???
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    logger.info(f"Device where I am running: {device}")

    # load dataset and build dataloaders
    train_loader, val_loader = build_data_loaders(
        dataset_filepath, batch_size, device
    )
    logger.info("Split into training set and testing set.")

    # get first batch of Data from Dataloader
    first_batch = next(iter(train_loader))

    # previously called max_n_atoms
    n_atoms_per_structure = first_batch[0].num_nodes

    # it was called before as n_atom_types
    n_elements = first_batch[0].num_node_features

    # neural network models
    encoder = SimpleNetwork(
        irreps_in=f"{n_elements}x0e",  # based on n_elements, ex: "6x0e"
        irreps_out="1x0e",
        max_radius=max_radius_e3nn,
        num_neighbors=15,
        num_nodes=n_atoms_per_structure,
        layers=n_layers_e3nn,
        lmax=lmax_e3nn,
        mul=mul_e3nn,
        pool_nodes=False,
    ).to(device)

    autoencoder = Autoencoder_cmap(
        encoder,  # simple network from above
        n_atoms_per_structure,
        n_elements,
        batch_size,
        activ_func_mlp_cmap,
        n_nodes_mlp_cmap,
        n_layers_mlp_cmap,
        device,
    ).to(device)

    # create new folder where to save autoencoder
    # hyper-parameters and parameters
    autoencoder_dir = create_new_autoencoder_folder()
    logger.info(f"Make directory for model: {autoencoder_dir}")

    # establish which hyperparameters you want to save in the folder
    hyperparams = {
        "max_radius_e3nn": max_radius_e3nn,
        "n_layers_e3nn": n_layers_e3nn,
        "lmax_e3nn": lmax_e3nn,
        "mul_e3nn": mul_e3nn,
        "n_elements": n_elements,
        "n_atoms_per_structure": n_atoms_per_structure,
        "batch_size": batch_size,
        "activ_func_mlp_cmap": activ_func_mlp_cmap,
        "n_nodes_mlp_cmap": n_nodes_mlp_cmap,
        "n_layers_mlp_cmap": n_layers_mlp_cmap,
        "n_epochs": n_epochs,
        "learning_rate": learning_rate,
    }

    # save hyperparameters into a text file
    save_hyperparams(autoencoder_dir, hyperparams)
    logger.info(f"Save model hyper-parameters into: {autoencoder_dir}")

    # initialize wadn project
    # wandb.login(key= ADD your key)
    # wandb.init(project="tf-dna", config={**hyperparams})

    # instantiate Adam optimizer
    optimizer = Adam(
        autoencoder.parameters(), lr=learning_rate, weight_decay=weight_decay
    )

    # set gradients to zero
    optimizer.zero_grad(set_to_none=True)

    # instantiate scheduler
    # TODO not used ask Riccardo
    # scheduler = ReduceLROnPlateau(
    #     optimizer, "min", factor=0.9, patience=1, verbose=True
    # )

    # instantiate criterion, loss function is MSE
    criterion = torch.nn.MSELoss()

    # initialize arrays of loss values across epochs
    train_loss_across_epochs, val_loss_across_epochs = [], []

    for epoch in tqdm(range(n_epochs)):
        # training loop
        autoencoder.train()
        epoch_loss_train = 0.0  # Somma delle training loss nella epoca

        for batch_idx, batch in enumerate(train_loader):
            batch_distances = []  # real atom distances

            # i-th structure in batch
            for i in range(len(batch["ptr"]) - 1):
                # atom distances of the i-th structure
                # AKA contact map AKA cmap
                structure_distances = torch.cdist(
                    batch["pos"][batch["ptr"][i]: batch["ptr"][i + 1]],
                    batch["pos"][batch["ptr"][i]: batch["ptr"][i + 1]],
                ).to(device)

                # Padding per rendere la matrice quadrata
                # useless since interfaces have the same shape
                # structure_distances = pad(
                #     structure_distances,
                #     (
                #         0,
                #         n_atoms_per_structure - structure_distances.size(0),
                #     ),
                #     value=max_cmap_distance,
                # ).to(device)

                # Aggiunge la distanza alla lista delle distanze
                batch_distances.append(structure_distances)

            # set previous gradients to zero
            optimizer.zero_grad()

            # forward pass
            # prediction of distances for every structure in batch
            out = autoencoder(batch, len(batch))

            # Inizializza la loss per il batch corrente
            loss = 0

            # Calcola la MSE tra le distanze predette e quelle reali
            for i in range(len(batch["ptr"]) - 1):
                mse_loss = criterion(out[i], batch_distances[i].reshape(-1))
                loss += torch.sqrt(mse_loss)

            loss.backward()  # Calcola i gradienti
            optimizer.step()  # Aggiorna i pesi del modello
            optimizer.zero_grad(set_to_none=True)  # Reset dei gradienti
            # Media delle loss nella epoca
            epoch_loss_train += loss.item() / len(batch)

        # log into wandb
        # wandb.log({"train_loss": epoch_loss_train/len(train_loader)})

        # Calcola la media della perdita di addestramento per l'epoca corrente
        # train_loss_epochs.append(tempo_train_loss / len(train_loader))
        epoch_loss_train /= len(train_loader)
        logger.info(f"train_loss: {epoch_loss_train}")
        train_loss_across_epochs.append(epoch_loss_train)

        # validation loop
        autoencoder.eval()
        epoch_loss_val = 0.0  # Somma delle validation loss nella epoca

        # Disabilita il calcolo dei gradienti per risparmiare memoria
        with torch.no_grad():
            # Itera su ogni batch nel set di validazione
            for batch_idx, batch in enumerate(val_loader):
                batch_distances = []  # real atom distances

                # i-th structure in batch
                for i in range(len(batch["ptr"]) - 1):
                    # atom distances of the i-th structure
                    structure_distances = torch.cdist(
                        batch["pos"][batch["ptr"][i]: batch["ptr"][i + 1]],
                        batch["pos"][batch["ptr"][i]: batch["ptr"][i + 1]],
                    ).to(device)

                    # Padding per rendere la matrice quadrata
                    # useless since interfaces have the same shape
                    # structure_distances = pad(
                    #     structure_distances,
                    #     (
                    #         0,
                    #         n_atoms_per_structure - \
                    # structure_distances.size(0),
                    #     ),
                    #     value=max_cmap_distance,
                    # ).to(device)

                    # Aggiunge la distanza alla lista delle distanze
                    batch_distances.append(structure_distances)

                # forward pass
                # prediction of distances for every structure in batch
                out = autoencoder(batch, len(batch))

                # Inizializza la loss per il batch corrente
                loss = 0

                # Calcola la MSE tra le distanze predette e quelle reali
                for i in range(len(batch["ptr"]) - 1):
                    mse_loss = criterion(
                        out[i], batch_distances[i].reshape(-1)
                    )
                    loss += torch.sqrt(mse_loss)

                optimizer.zero_grad(set_to_none=True)  # Reset dei gradienti
                # Media delle loss nella epoca
                epoch_loss_val += loss.item() / len(batch)

            # log into wandb
            # wandb.log({"val_loss": epoch_loss_val/len(val_loader)})

            # Calcola la media della perdita di addestramento per l'epoca
            # corrente
            epoch_loss_val /= len(val_loader)
            logger.info(f"val_loss: {epoch_loss_val}")
            val_loss_across_epochs.append(epoch_loss_val)

            # Save model if it is the first epoch, otherwise save it only if
            # epoch loss is smaller then previous epoch:
            if epoch == 0:
                min_train_loss = epoch_loss_train
                min_val_loss = epoch_loss_val
                torch.save(
                    autoencoder.state_dict(),
                    f"{autoencoder_dir}/model_train.pt",
                )
                torch.save(
                    autoencoder.state_dict(),
                    f"{autoencoder_dir}/model_train.pt",
                )
                np.save(
                    f"{autoencoder_dir}/train_loss.npy",
                    train_loss_across_epochs,
                )
                np.save(
                    f"{autoencoder_dir}/val_loss.npy", val_loss_across_epochs
                )
            else:
                min_train_loss, min_val_loss = save_model(
                    min_train_loss,
                    min_val_loss,
                    autoencoder,
                    epoch_loss_train,
                    epoch_loss_val,
                    autoencoder_dir,
                )
                np.save(
                    f"{autoencoder_dir}/train_loss.npy",
                    train_loss_across_epochs,
                )
                np.save(
                    f"{autoencoder_dir}/val_loss.npy", val_loss_across_epochs
                )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
