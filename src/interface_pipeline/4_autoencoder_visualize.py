"""Visualize classifier models."""

import sys
import logging
from pathlib import Path
import click

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


@click.command()
@click.option(
    "--autoencoder_dir",
    type=click.Path(exists=True),
    help="Directory with autoencoder model, summaries etc.",
)
@click.option(
    "--figures_dir",
    type=click.Path(exists=True),
    help="Output directory with figures.",
)
def main(autoencoder_dir, figures_dir):
    """Visualize autoencoder models."""
    train_loss_filepath = Path(autoencoder_dir) / "train_loss.npy"
    val_loss_filepath = Path(autoencoder_dir) / "val_loss.npy"

    train_loss = np.load(train_loss_filepath)
    val_loss = np.load(val_loss_filepath)

    data = [("Training loss", loss, i) for i, loss in enumerate(train_loss)]
    data = data + [
        ("Validation loss", loss, i) for i, loss in enumerate(val_loss)
    ]
    data = pd.DataFrame(data, columns=["Loss", "MSE", "Epoch Number"])

    plt.figure(figsize=(5, 5))
    sns.lineplot(data, x="Epoch Number", y="MSE", hue="Loss")
    # plt.title("Training and Validation Loss (MSE)")
    out_filepath = f"{figures_dir}/loss.pdf"
    plt.savefig(out_filepath, format="pdf")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
