"""Autoencoder Milbinding."""

# from e3nn.nn.models.v2103.gate_points_networks import SimpleNetwork
from torch.nn import Module, Linear, ELU, Tanh, Sequential
from torch import zeros, triu_indices
import warnings

warnings.filterwarnings("ignore")


class Autoencoder_cmap(Module):
    """Autoencoder Milbinding."""

    def __init__(
        self,
        SN_cmap,
        max_n_atoms,  # correggere in max n atom
        num_atoms_type,  # correggere in n atom types
        batch_size,
        activation_func_mlp_cmap,  # ELU o Tanh
        n_nodes_mlp_cmap,  # Numero di nodi in ogni livello della rete
        n_layers_mlp_cmap,  # Numero di livelli del Multi Layer Perceptron
        device,  # cpu o gpu
    ):

        super(Autoencoder_cmap, self).__init__()
        self.SN_cmap = SN_cmap

        # sequenza di moduli MLP Multi Layer Perceptron
        modules = []

        if activation_func_mlp_cmap == "ELU":
            modules.append(Linear(max_n_atoms, n_nodes_mlp_cmap))
            modules.append(ELU())
        if activation_func_mlp_cmap == "Tanh":
            modules.append(Linear(max_n_atoms, n_nodes_mlp_cmap))
            modules.append(Tanh())

        for i in range(n_layers_mlp_cmap - 1):
            if activation_func_mlp_cmap == "ELU":
                modules.append(Linear(n_nodes_mlp_cmap, n_nodes_mlp_cmap))
                modules.append(ELU())
            if activation_func_mlp_cmap == "Tanh":
                modules.append(Linear(n_nodes_mlp_cmap, n_nodes_mlp_cmap))
                modules.append(Tanh())
        modules.append(
            Linear(n_nodes_mlp_cmap, int(max_n_atoms * (max_n_atoms + 1) / 2))
        )

        self.mlp_cmap = Sequential(*modules)

        # other arguments
        self.num_atoms_type = num_atoms_type
        self.max_n_atoms = max_n_atoms
        self.batch_size = batch_size
        self.device = device

    def forward(self, item, batch_size):
        """Forward step for training."""
        # contact map
        # perche x??
        x_cmap = self.SN_cmap(item)

        # matrice di padding per gestire la dimensione variabile degli input
        pad = zeros(batch_size, self.max_n_atoms).to(self.device)

        for i in range(batch_size):
            pad[i, 0: item["ptr"][i + 1] - item["ptr"][i]] = x_cmap[
                item["ptr"][i]: item["ptr"][i + 1]
            ].squeeze()
        x1_diag = self.mlp_cmap(pad)

        # si occupa della simmetria della matrice
        x1 = zeros(batch_size, self.max_n_atoms, self.max_n_atoms).to(
            self.device
        )
        ii, jj = triu_indices(self.max_n_atoms, self.max_n_atoms)
        for i in range(batch_size):
            x1[i, ii, jj] = x1_diag[i]
            x1[i].T[ii, jj] = x1_diag[i]

        return x1.reshape(batch_size, self.max_n_atoms * self.max_n_atoms)
