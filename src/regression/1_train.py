"""Classification: predicting interface-DNA binding."""

import sys
import logging
from pathlib import Path
import click

import pickle
import pandas as pd

from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Lasso
from sklearn.linear_model import Ridge

# from sklearn.svm import SVR
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor


# local imports
from utils import mkdir_new_model
from utils import save_hyperparams


def train_model(X, y, model, model_name, bins_model_dir, param_grid) -> None:
    """Train and validate given model."""
    # make new directory for model
    model_dir = mkdir_new_model(model_name, models_dir=bins_model_dir)

    # hyperparameters tuning and cross-validation
    grid_search = GridSearchCV(
        model,
        param_grid=param_grid,
        scoring="neg_mean_squared_error",
        cv=5,
        n_jobs=8,
        verbose=4,
    )
    grid_search.fit(X, y)

    # save best model from the hyperparameters tuning and cross-validation
    model = grid_search.best_estimator_
    with open(f"{model_dir}/model_train.pkl", "wb") as f:
        pickle.dump(model, f)

    # save best combination of hyperparameters in the grid
    best_hyperparams = grid_search.best_params_
    save_hyperparams(model_dir, dict_hyperparams=best_hyperparams)

    # save cross-validation results
    cv_results = pd.DataFrame(grid_search.cv_results_)
    cv_results.to_csv(f"{model_dir}/summary.csv")


# 1CMA
@click.command()
@click.option(
    "--dataset_filepath",
    type=click.Path(exists=True),
    help="Filepath to dataset for classifers, in .csv format.",
)
@click.option(
    "--regressors_dir",
    type=click.Path(exists=True),
    help="Filepath to saved regressor models.",
)
def main(dataset_filepath, regressors_dir):
    """Regression: predicting interface-DNA binding Delta G."""
    # our data
    df = pd.read_csv(dataset_filepath, header=0, index_col=0)

    # extract PDB ids
    df["pdb_id"] = df["interface_id"].apply(
        lambda interface_id: interface_id.split(":")[0]
    )

    # data with Delta G
    test_df = pd.read_excel("data/raw/Supplementary_Table.xlsx", index_col=0)
    test_df.columns = [
        "suppl_id",
        "class",
        "protein_name",
        "dna_name",
        "T(k)",
        "Kd",
        "delta_G",
        "pubmed",
    ]
    test_df["pdb_id"] = test_df["suppl_id"].apply(
        lambda pdb_id: pdb_id.split("_")[0]
    )

    # filterinig data, test data not in train data
    df = df[~df["pdb_id"].isin(test_df["pdb_id"])]

    for bins in df["bins"].unique():
        # mkdir for binning value
        bins_model_dir = Path(f"{regressors_dir}/bins-{bins}/")
        bins_model_dir.mkdir(parents=True, exist_ok=True)

        # select only features from DNA and interfaces
        X = df.loc[:, "dna-0":"y"]

        # select X based on binnings for interface fingerprints
        X = df[df["bins"] == bins]
        X = X.drop(["bins", "pdb_id"], axis=1)

        # predictor feature
        y = X.pop("y")

        # select only features from DNA and interfaces
        X = X.loc[:, "dna-0":]

        # drop all columns with all NA, so the columns used by bigger bins
        X = X.dropna(axis=1, how="all")
        logger.info(f"X feature set of shape = {X.shape}")

        param_grid = {
            "n_neighbors": [3, 5, 10, 25, 50, 100],
        }
        logger.info(f"Training KNN Regressor model with bins={bins} ...")
        train_model(
            X=X,
            y=y,
            model=KNeighborsRegressor(),
            param_grid=param_grid,
            model_name="knn",
            bins_model_dir=bins_model_dir,
        )
        logger.info(f"Finished training KNN Regressor model with bins={bins}.")

        param_grid = {
            "fit_intercept": [False, True],
        }
        logger.info(f"Training Linear Regressor model with bins={bins} ...")
        train_model(
            X=X,
            y=y,
            model=LinearRegression(),
            param_grid=param_grid,
            model_name="linear",
            bins_model_dir=bins_model_dir,
        )
        logger.info(
            f"Finished training Linear Regressor model with bins={bins}."
        )

        param_grid = {"alpha": [0.1, 1.0, 10.0, 100.0]}
        logger.info(f"Training Lasso Regressor model with bins={bins} ...")
        train_model(
            X=X,
            y=y,
            model=Lasso(),
            param_grid=param_grid,
            model_name="lasso",
            bins_model_dir=bins_model_dir,
        )
        logger.info(
            f"Finished training Lasso Regressor model with bins={bins}."
        )

        param_grid = {"alpha": [0.1, 1.0, 10.0, 100.0]}
        logger.info(f"Training Ridge Regressor model with bins={bins} ...")
        train_model(
            X=X,
            y=y,
            model=Ridge(),
            param_grid=param_grid,
            model_name="ridge",
            bins_model_dir=bins_model_dir,
        )
        logger.info(
            f"Finished training Ridge Regressor model with bins={bins}."
        )

        #         param_grid = {
        #             'kernel': ('linear', 'rbf', 'poly'),
        #             "C": [0.001, 0.1, 1, 10, 100, 1000],
        #             'gamma': [10, 1, 0.1, 0.01, 0.001, 0.0001, 1e-4, 1e-7],
        #         }
        #         logger.info(
        #             f"Training Support Vector Regressor model with \
        # bins={bins} ...")
        #         train_model(
        #             X=X,
        #             y=y,
        #             model=SVR(),
        #             param_grid=param_grid,
        #             model_name="svr",
        #             bins_model_dir=bins_model_dir,
        #         )
        #         logger.info(
        #             f"Finished training Support Vector Regressor model with \
        # bins={bins}.")

        param_grid = {
            "n_estimators": [25, 50, 100, 150, 200],
            "max_features": ["sqrt", "log2", None],
            "max_depth": [3, 6, 7, 9],
            "max_leaf_nodes": [3, 6, 9],
        }
        logger.info(
            f"Training Random Forest Regressor model with bins={bins} ..."
        )
        train_model(
            X=X,
            y=y,
            model=RandomForestRegressor(),
            param_grid=param_grid,
            model_name="randomforest",
            bins_model_dir=bins_model_dir,
        )
        logger.info(
            f"Finished training Random Forest Regressor model with \
bins={bins}."
        )

        param_grid = {
            "n_estimators": [25, 50, 100, 150, 200],
            "learning_rate": [0.01, 0.1, 0.2],
            "max_depth": [3, 5, 7, 9],
            "max_leaf_nodes": [3, 6, 9],
        }
        logger.info(
            f"Training Gradient Boosting Regressor model with \
bins={bins} ..."
        )
        train_model(
            X=X,
            y=y,
            model=GradientBoostingRegressor(),
            param_grid=param_grid,
            model_name="gboost",
            bins_model_dir=bins_model_dir,
        )
        logger.info(
            f"Finished training Gradient Boosting Regressor model with \
bins={bins}."
        )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
