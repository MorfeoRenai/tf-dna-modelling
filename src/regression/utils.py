"""Utils for models."""

import os
from pathlib import Path
import click
import pickle


def validate_positive_int(ctx, param, value):
    """
    Validate positive integer arguments/options.

    Function used by `click`, argument parsing.
    """
    if value <= 0:
        raise click.BadParameter(f"{param} must be a positive integer.")
    return value


def save_hyperparams(model_dir, dict_hyperparams):
    """
    Save the hyperparameters of the training.

    Save both as pickle dump file and text file.
    """
    txt_filepath = f"{model_dir}/hyperparams.txt"
    dump_filepath = f"{model_dir}/hyperparams.pkl"

    with open(txt_filepath, "w") as f:
        for param_name, param_value in dict_hyperparams.items():
            f.write(f"{param_name}: {param_value}\n")

    with open(dump_filepath, "wb") as f:
        pickle.dump(dict_hyperparams, f)


def load_hyperparams(model_dir):
    """Load the hyperparameters of the training."""
    hyperparams_filepath = f"{model_dir}/hyperparams.txt"

    with open(hyperparams_filepath, "r") as f:
        lines = f.readlines()
        lines = [line.strip() for line in lines]
        hyperparams_list = [line.split(": ") for line in lines]

        hyperparams = {}
        for name, value in hyperparams_list:
            if value.isdigit():
                hyperparams[name] = int(value)
            else:
                try:
                    hyperparams[name] = float(value)
                except ValueError:
                    hyperparams[name] = value

    return hyperparams


def create_new_autoencoder_folder(models_dir="models"):
    """
    Make new folder for saving model.

    In there save loss, hyperparameter and trainable parameters.
    """
    # TODO generalizzare per ogni modello, non solo autoncoder
    # rinominare mkdir_new_model

    # get all the folders already present in the models folder
    existing_dirs = [
        d
        for d in os.listdir(models_dir)
        if os.path.isdir(os.path.join(models_dir, d))
    ]

    # Look only at the folder named 'autoencoder_n' which is the pattern used
    # to save autoencoders
    autoencoder_dirs = [
        d for d in existing_dirs if d.startswith("autoencoder_")
    ]

    # extract the last numeration of the previously saved models
    numbers = [
        int(d.split("_")[-1])
        for d in autoencoder_dirs
        if d.split("_")[-1].isdigit()
    ]

    # Determine the next available number for autoencoder_n
    # Start with 1 if no autoencoder folders exist
    next_n = max(numbers) + 1 if numbers else 1

    # Create the new directory for autoencoder_n
    new_folder = f"autoencoder_{next_n}"
    new_folder_path = os.path.join(models_dir, new_folder)
    os.makedirs(new_folder_path)

    return new_folder_path


def mkdir_new_model(model_name, models_dir="models"):
    """
    Make new folder for saving model.

    In there save loss, hyperparameter and trainable parameters.
    """
    # Look only at the folders named 'autoencoder_n' which is the pattern used
    # to save autoencoders
    model_directories = [
        filepath
        for filepath in Path(models_dir).iterdir()
        if filepath.is_dir() and model_name in filepath.stem
    ]

    # extract the last numeration of the previously saved models
    numbers = [
        int(model_dir.name.split("-")[-1])
        for model_dir in model_directories
        if model_dir.name.split("-")[-1].isdigit()
    ]

    # Determine the next available number for autoencoder_n
    # Start with 0 if no folders with the same model name exist
    next_n = max(numbers) + 1 if numbers else 0

    # Create the new directory for autoencoder_n
    new_model_dir = Path(models_dir) / f"{model_name}-{next_n}"
    new_model_dir.mkdir(parents=True, exist_ok=True)

    return new_model_dir
