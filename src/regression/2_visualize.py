"""Visualize regressor models."""

import sys
import logging
from pathlib import Path
import click

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pickle

# from sklearn.model_selection import train_test_split

from scipy.stats import spearmanr


@click.command()
@click.option(
    "--dataset_filepath",
    type=click.Path(exists=True),
    help="Filepath to dataset for classifers, in .csv format.",
)
@click.option(
    "--regressors_dir",
    type=click.Path(exists=True),
    help="Directory with regressor models, summaries etc.",
)
@click.option(
    "--figures_dir",
    type=click.Path(exists=True),
    help="Output directory with figures.",
)
def main(dataset_filepath, regressors_dir, figures_dir):
    """Visualize regressor models."""
    # meta-summary: summary of summaries
    meta_summary = []

    for filepath in Path(regressors_dir).iterdir():
        if not (filepath.is_dir() and "bins" in filepath.name):
            continue
        _, bins = filepath.name.split("-")
        bins = int(bins)
        for filepath2 in filepath.iterdir():
            if not filepath.is_dir():
                continue
            model_name, model_num = filepath2.name.split("-")
            model_num = int(model_num)
            summary_filepath = filepath2 / "summary.csv"
            df = pd.read_csv(summary_filepath, header=0, index_col=0)

            # take the best model
            df = df[df["rank_test_score"] == 1]

            # take the cross validated score (neg mse in this case)
            # by slicing until "mean_test_score"
            # it takes a variable number
            # of splits/cv values, instead of hard-coding it
            df = df.loc[:, "split0_test_score":"mean_test_score"]
            df = df.drop("mean_test_score", axis=1)

            for metric in df.values.reshape(-1):
                row = (bins, model_name, model_num, metric)
                meta_summary.append(row)

    # meta-summary: summary of summaries
    meta_summary = pd.DataFrame(
        meta_summary, columns=["bins", "model_name", "model_num", "mse"]
    ).sort_values(by="bins")

    # box plots
    plt.figure()
    g = sns.catplot(
        data=meta_summary,
        y="mse",
        x="bins",
        hue="model_name",
        kind="box",
        height=5,
        aspect=1,
    )
    g.set(
        xlabel="Bin number for interface fingerprint",
        ylabel="Cross-validated -MSE",
    )
    # g.fig.subplots_adjust(top=0.9)
    # g.fig.suptitle(
    #     "Cross-Validated ROC AUC for Every regressor-Binning Permutation"
    # )
    out_filepath = f"{figures_dir}/cs_mse-box.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(
        f"Boxplots of cross-validated neg mse for every regressor at \
{out_filepath}"
    )

    # box plots without linear
    meta_summary = meta_summary[meta_summary["model_name"] != "linear"]
    plt.figure()
    g = sns.catplot(
        data=meta_summary,
        y="mse",
        x="bins",
        hue="model_name",
        kind="box",
        height=5,
        aspect=1,
    )
    g.set(
        xlabel="Bin number for interface fingerprint",
        ylabel="Cross-validated -MSE",
    )
    out_filepath = f"{figures_dir}/cs_mse-box-2.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(
        f"Boxplots of cross-validated neg MSE for every regressor at \
{out_filepath}"
    )

    # bar plots without linear
    plt.figure()
    g = sns.catplot(
        data=meta_summary,
        y="mse",
        x="bins",
        hue="model_name",
        kind="bar",
        height=5,
        aspect=1,
    )
    g.set(
        xlabel="Bin number for interface fingerprint",
        ylabel="Cross-validated -MSE",
    )
    out_filepath = f"{figures_dir}/cs_mse-bar-2.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(
        f"Barplots of cross-validated neg MSE for every regressor at \
{out_filepath}"
    )

    bins = 5
    with open(
        f"{regressors_dir}/bins-{bins}/gboost-0/model_train.pkl", "rb"
    ) as f:
        model = pickle.load(f)
    logger.info(f"Loaded best model with bins={bins}")

    # our data
    df = pd.read_csv(dataset_filepath, header=0, index_col=0)

    # extract PDB ids
    df["pdb_id"] = df["interface_id"].apply(
        lambda interface_id: interface_id.split(":")[0]
    )

    # select only features from DNA and interfaces
    X = df.loc[:, "dna-0":"y"]
    # X = X.drop("pdb_id", axis=1)

    # select X based on binnings for interface fingerprints
    X = X[X["bins"] == bins]
    X = X.drop("bins", axis=1)

    # predictor feature
    y = X.pop("y")

    # drop all columns with all NA, so the columns used by bigger bins
    X = X.dropna(axis=1, how="all")
    logger.info(f"X feature set of shape = {X.shape}")

    # fit
    model.fit(X, y)
    logger.info(f"Fitted best model with bins={bins}")

    # predictions
    y_pred = model.predict(X)
    data = pd.DataFrame({
        "y_real": y,
        "y_pred": y_pred,
    })

    # predictions
    plt.figure()
    g = sns.histplot(
        data=data,
        x="y_pred",
        hue="y_real",
        # stat="frequency",
        bins=50,
        kde=True,
    )
    logger.info(
        "Histogram of regressor predictions, grouped by bindig flag."
    )
    out_filepath = f"{figures_dir}/predictions-hist-train.pdf"
    plt.savefig(out_filepath, format="pdf")

    # data with Delta G
    df2 = pd.read_excel("data/raw/Supplementary_Table.xlsx", index_col=0)
    df2.columns = [
        "suppl_id",
        "class",
        "protein_name",
        "dna_name",
        "T(k)",
        "Kd",
        "delta_G",
        "pubmed",
    ]
    df2["pdb_id"] = df2["suppl_id"].apply(lambda pdb_id: pdb_id.split("_")[0])

    # merging data
    df3 = pd.merge(df, df2, on="pdb_id", how="inner")

    # select X based on binnings for interface fingerprints
    X = df3[df3["bins"] == bins]

    # predictor feature
    y = X.pop("delta_G")

    # drop the df2 metadata
    X = X.drop(
        [
            "bins",
            "y",
            "pdb_id",
            "suppl_id",
            "class",
            "protein_name",
            "dna_name",
            "T(k)",
            "Kd",
            "pubmed",
        ],
        axis=1,
    )

    # select only features from DNA and interfaces
    X = X.loc[:, "dna-0":]

    # drop all columns with all NA, so the columns used by bigger bins
    X = X.dropna(axis=1, how="all")

    # predictions
    y_pred = model.predict(X)
    corr = spearmanr(y, y_pred)

    # scatter plot of real Delta G prediction
    plt.figure()
    g = sns.scatterplot(
        data=pd.DataFrame({"y_real": y, "y_pred": y_pred}),
        x="y_real",
        y="y_pred",
        legend=False,
    )
    plt.legend(
        # title='Smoker',
        # loc='upper left',
        labels=[f"Spearnman Correlation = {corr[0]:.3f}"]
    )
    g.set(
        xlabel="Real Test-set Delta G",
        ylabel="Predicted Delta G",
    )
    logger.info(
        "Scatter plot of regressor predictions against real Delta G."
    )
    out_filepath = f"{figures_dir}/predictions-scatter-test.pdf"
    plt.savefig(out_filepath, format="pdf")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
