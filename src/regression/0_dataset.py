"""Make dataset from DNA sequences bound by interfaces."""

import sys
import logging
from pathlib import Path
import click

import pickle
import pandas as pd


@click.command()
@click.option(
    "--dna_dataset_filepath",
    type=click.Path(exists=True),
    help="Bound DNA strings filepath, in .csv format.",
)
@click.option(
    "--fingerprints_dataset_filepath",
    type=click.Path(exists=True),
    help="Interfaces fingerprints, in binary dump format.",
)
@click.option(
    "--output_filepath",
    type=click.Path(),
    help="Output filepath, dataset in .csv format.",
)
def main(dna_dataset_filepath, fingerprints_dataset_filepath, output_filepath):
    """Finalize dataset for classification."""
    # read csv with interfaces ids and sequences etc
    dna_df = pd.read_csv(dna_dataset_filepath, header=0, index_col=0)
    logger.info(f"Loaded DNA dataset {dna_dataset_filepath}")

    with open(fingerprints_dataset_filepath, "rb") as f:
        dict_dataset = pickle.load(f)
    logger.info(f"Loaded interface dump {fingerprints_dataset_filepath}")

    interface_df = []
    for d in dict_dataset:
        interface_id = d["interface_id"]
        fingerprint_dict = d["fingerprints"]
        for bins, fingerprint in fingerprint_dict.items():
            fingerprint = fingerprint.reshape(-1)
            row = [interface_id] + [bins] + list(fingerprint)
            interface_df.append(row)
    interface_df = pd.DataFrame(interface_df)
    interface_df.columns = (
        ["interface_id"]
        + ["bins"]
        + [f"interface_{i}" for i in range(interface_df.shape[1] - 2)]
    )
    logger.info("Dump processed into dataframe.")

    # inner join of two dataset: DNA and interfaces
    final_df = pd.merge(dna_df, interface_df, on="interface_id", how="inner")

    # move around the y vector
    y = final_df.pop("y")
    final_df["y"] = y

    final_df.to_csv(output_filepath)
    logger.info(
        f"Saved final dataset with both encoded DNA sequences and interface \
fingerprints, at {output_filepath}"
    )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
