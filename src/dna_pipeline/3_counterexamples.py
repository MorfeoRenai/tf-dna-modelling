"""Try base pairing on assembly structures."""

import sys
import logging
from pathlib import Path
import click

import pandas as pd
import random
from scipy.spatial.distance import hamming


def generate_counterexample(seq, nucleotide_set="CGTA", min_dist=0.8):
    """Generate sequence of DNA with enough hamming distance from original."""
    seq = list(seq)
    dist = 0
    while dist < min_dist:
        random_seq = [random.choice(nucleotide_set) for _ in seq]
        dist = hamming(seq, random_seq)
    random_seq = "".join(random_seq)
    return random_seq


@click.command()
@click.option(
    "--input_filepath",
    type=click.Path(exists=True),
    help="Filepath to dataset with interim dataset, in .csv format.",
)
@click.option(
    "--output_filepath",
    type=click.Path(),
    help="Filepath to output DNA sequences bound by TFs, in .csv format.",
)
def main(input_filepath, output_filepath):
    """Build dataset of atom coordinates from interfaces."""
    # read csv with interfaces ids and sequences
    df = pd.read_csv(input_filepath, header=0, index_col=0)
    logger.info(f"Loaded dataset {input_filepath}")

    # counterexample dataframe
    ce_df = df[["interface_id", "description", "nakb_protein_annotations"]]
    ce_df["seq"] = df["seq"].apply(generate_counterexample)

    # flag for real vs counterexamples
    ce_df["is_real"] = False

    ce_df.to_csv(output_filepath)
    logger.info(
        f"Saved filtered dataset with both experimental and generated DNA \
sequences, at {output_filepath}"
    )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
