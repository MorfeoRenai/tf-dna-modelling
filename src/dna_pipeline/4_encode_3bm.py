"""Make dataset from DNA sequences bound by interfaces."""

import sys
import logging
from pathlib import Path
import click

import numpy as np
import pandas as pd


def three_body_matrix(seq):
    """Compute the three-body matrix for a DNA sequence."""
    # Define the bases and initialize the 4x4x4 matrix
    bases = ["A", "T", "C", "G"]
    base_indices = {base: idx for idx, base in enumerate(bases)}
    matrix = np.zeros((4, 4, 4), dtype=int)

    # Iterate through the sequence and count adjacent triplets
    for i in range(len(seq) - 2):
        base1 = seq[i]
        base2 = seq[i + 1]
        base3 = seq[i + 2]
        if (
            base1 in base_indices
            and base2 in base_indices
            and base3 in base_indices
        ):
            idx1 = base_indices[base1]
            idx2 = base_indices[base2]
            idx3 = base_indices[base3]
            matrix[idx1, idx2, idx3] += 1

    return matrix


@click.command()
@click.option(
    "--real_dataset_filepath",
    type=click.Path(exists=True),
    help="Bound DNA strings filepath, in .csv format.",
)
@click.option(
    "--counterexample_dataset_filepath",
    type=click.Path(exists=True),
    help="Generated DNA strings filepath, in .csv format.",
)
@click.option(
    "--output_filepath",
    type=click.Path(),
    help="Output filepath, dataset in .csv format.",
)
def main(
    real_dataset_filepath, counterexample_dataset_filepath, output_filepath
):
    """Build dataset of atom coordinates from interfaces."""
    # read csv with interfaces ids and sequences
    real_df = pd.read_csv(real_dataset_filepath, header=0, index_col=0)
    logger.info(f"Loaded dataset {real_dataset_filepath}")

    # encode padded sequences that will bind to the interface
    encoded_df = []
    for seq in real_df["seq"]:
        encoded_seq = three_body_matrix(seq)

        # reshape into 1D vector
        encoded_seq = encoded_seq.reshape(-1)
        encoded_df.append(encoded_seq)

    # cast into dataframe
    encoded_df = pd.DataFrame(encoded_df)
    encoded_df.columns = [f"dna-{i}" for i in range(encoded_df.shape[1])]
    logger.info("Three body matrix encoding of DNA sequences.")

    # concatenate DNA info dataframe with encoded DNA sequences dataframe
    # these sequence should be posively predicted
    # concatenate columns
    positive_df = pd.concat([real_df, encoded_df], axis=1)
    positive_df["y"] = 1

    # read csv with interfaces ids and sequences
    # counterexamples
    ce_df = pd.read_csv(counterexample_dataset_filepath, header=0, index_col=0)
    logger.info(f"Loaded dataset {counterexample_dataset_filepath}")

    # encode padded sequences that won't bind to the interface
    encoded_df = []
    for seq in ce_df["seq"]:
        encoded_seq = three_body_matrix(seq)

        # reshape into 1D vector
        encoded_seq = encoded_seq.reshape(-1)
        encoded_df.append(encoded_seq)

    # cast into dataframe
    encoded_df = pd.DataFrame(encoded_df)
    encoded_df.columns = [f"dna-{i}" for i in range(encoded_df.shape[1])]
    logger.info("Three body matrix encoding of DNA sequences.")

    # concatenate DNA info dataframe with encoded DNA sequences dataframe
    # these sequence should be negatively predicted
    # concatenate columns
    negative_df = pd.concat([ce_df, encoded_df], axis=1)
    negative_df["y"] = 0

    # final dataframe, concatenate positive and negative dataframes on rows
    final_df = (
        pd.concat([positive_df, negative_df], axis=0)
        .sort_values("interface_id")
        .reset_index(drop=True)
    )

    final_df.to_csv(output_filepath)
    logger.info(
        f"Saved final dataset with both experimental and generated DNA \
sequences, at {output_filepath}"
    )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
