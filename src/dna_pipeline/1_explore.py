"""Exlore DNA sequences bound by interfaces."""

import sys
import logging
from pathlib import Path
import click

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


def get_color_mapping_by_count(df, category_column, palette_name="viridis"):
    """
    Map di colori basato sui conteggi delle categorie in un DataFrame.

    Parametri:
    - data: DataFrame contenente le categorie.
    - category_column: str, nome della colonna delle categorie.
    - palette_name: str, nome della palette (es. "viridis").

    Ritorna:
    - color_mapping: dict, dizionario che mappa ogni categoria a un colore.
    """
    # Calcola il conteggio per ciascuna categoria
    counts = df[category_column].value_counts().reset_index()
    counts.columns = [category_column, "Count"]

    # Normalizza i conteggi tra 0 e 1
    norm = counts["Count"] / counts["Count"].max()

    # Ottieni i colori dalla palette scelta
    palette = plt.get_cmap(palette_name)
    colors = [palette(value) for value in norm]

    # Crea il mapping di colori per ogni categoria
    color_mapping = dict(zip(counts[category_column], colors))

    return color_mapping


def get_color_mapping_by_measure(
    df, category_column, measure_column, palette_name="viridis"
):
    """
    Map di colori basato sui valori medi di una misura per ciascuna categoria.

    Parametri:
    - data: DataFrame contenente le categorie e la misura.
    - category_column: str, nome della colonna delle categorie.
    - measure_column: str, nome della colonna della misura.
    - palette_name: str, nome della palette (es. "viridis").

    Ritorna:
    - color_mapping: dict, mappa ogni categoria a un colore basato sui valori
      medi di misura.
    """
    # Calcola la media della misura per ciascuna categoria
    means = df.groupby(category_column)[measure_column].mean().reset_index()

    # Normalizza i valori medi tra 0 e 1
    norm = (means[measure_column] - means[measure_column].min()) / (
        means[measure_column].max() - means[measure_column].min()
    )

    # Ottieni i colori dalla palette scelta
    palette = plt.get_cmap(palette_name)
    colors = [palette(value) for value in norm]

    # Crea il mapping di colori per ogni categoria
    color_mapping = dict(zip(means[category_column], colors))

    return color_mapping


@click.command()
@click.option(
    "--input_filepath",
    type=click.Path(exists=True),
    help="Bound DNA strings filepath, in .csv format.",
)
@click.option(
    "--figures_dir",
    type=click.Path(exists=True),
    help="Output directory with figures.",
)
def main(input_filepath, figures_dir):
    """Explore dataset of atom coordinates from interfaces."""
    # read csv with interfaces ids and sequences
    dna_df = pd.read_csv(input_filepath, header=0, index_col=0)
    logger.info(f"Loaded dataset {input_filepath}")

    # set of unique atoms
    nucleotide_list = []
    for seq in dna_df["seq"]:
        nucleotide_list += seq.strip().split(" ")

    sns.set_theme(style="ticks")

    # nucleotides frequencies
    plt.figure()
    ax = sns.countplot(
        nucleotide_list,
        # palette="viridis",
    )
    # ax.set_title("Absolute Frequencies of Nucleotides")
    ax.set_xlabel("Count")
    out_filepath = f"{figures_dir}/nucleotides-count.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(f"Histogram countes by nucleotides, saved at {out_filepath}")

    # sequence lengths distribution
    plt.figure(figsize=(5, 5))
    ax = sns.histplot(
        data=dna_df,
        x="len_seq",
        kde=True,
        # color="#482966",
    )
    # ax.set_title("Distribution of Length of DNA Sequences Bound by Proteins")
    ax.set_xlabel("Length, measured in base-pairs")
    out_filepath = f"{figures_dir}/seq_len-hist.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(f"Histogram of sequence lengths, saved at {out_filepath}")

    # sequence lengths distribution
    plt.figure(figsize=(5, 5))
    ax = sns.histplot(
        data=dna_df,
        x="len_seq",
        kde=True,
        log_scale=True,
        # color="#482966",
    )
    # ax.set_title("Distribution of Length of DNA Sequences Bound by Proteins")
    ax.set_xlabel("Length, measured in base-pairs")
    out_filepath = f"{figures_dir}/seq_len-loghist.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(f"Histogram of sequence lengths, saved at {out_filepath}")

    # functional annotation counts
    color_mapping = get_color_mapping_by_count(
        dna_df, category_column="nakb_protein_annotations"
    )
    g = sns.catplot(
        data=dna_df,
        y="nakb_protein_annotations",
        kind="count",
        height=10,
        aspect=0.8,
        palette=color_mapping,
    )
    g.set(xlabel="Counts", ylabel="Protein Annotation")
    # g.fig.subplots_adjust(top=0.9)
    # g.fig.suptitle("Distribution of Functional Annotation Terms")
    out_filepath = f"{figures_dir}/annotation-count.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(
        f"Histogram counts by functional annotation, saved at {out_filepath}"
    )

    # boxenplots of len for functional annotation
    color_mapping = get_color_mapping_by_measure(
        dna_df,
        category_column="nakb_protein_annotations",
        measure_column="len_seq",
        palette_name="viridis_r",
    )
    g = sns.catplot(
        data=dna_df,
        x="len_seq",
        y="nakb_protein_annotations",
        kind="boxen",
        height=10,
        aspect=0.8,
        palette=color_mapping,
    )
    g.set(xlabel="Length, measured in base-pairs", ylabel="Protein Annotation")
    # g.fig.subplots_adjust(top=0.9)
#     g.fig.suptitle(
#         "Distributions of Length of DNA Sequences grouped by Functional \
# Annotation Terms"
#     )
    out_filepath = f"{figures_dir}/annotation-seq_len-boxen.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(
        f"Boxenplots of sequence lengths by functional annotation, saved at \
{out_filepath}"
    )

    # dna_df["nakb_protein_annotations"].unique()
    # ['rel homology', 'leucine zipper (bZIP)', 'helix-loop-helix (bHLH)',
    #  'Zinc finger C2H2', 'P53 like', 'nuclear receptor ZnC4',
    #  'homeodomain', 'winged helix/forkhead', 'transcription',
    #  'lambda repressor-like', 'MetR homology', 'STAT', 'MADS box',
    #  'transcription, transferase', 'TATA binding protein', 'HMG box',
    #  'trp repressor', 'runt', 'Zinc-coordinating', 'SMAD-like',
    #  'chromatin, HMG box', 'nuclease, transcription',
    #  'helicase, transcription', 'helix-turn-helix (HTH)',
    #  'oxidoreductase, transcription', 'toxin/antitoxin, transcription',
    #  'tetR family', 'RNA-binding transcription factor',
    #  'methylase, transcription', 'chromatin, winged helix/forkhead',
    #  'centromere, winged helix/forkhead', 'nucleosome',
    #  'hydrolase, transcription', 'chromatin, transcription',
    #  'telomere, transcription', 'polymerase, transcription',
    #  'recombinase', 'helicase', 'centromere, transcription',
    #  'chromatin', 'CRISPR-Cas, transcription', 'DNA replication/repair',
    #  'DNA-binding transcription factor (TF)', 'centromere',
    #  'DNA-binding transcription factor (TF), nucleosome']

    # explore some filtering options
    # by annotation
    selected_annotations = [
        "rel homology",
        "leucine zipper (bZIP)",
        "helix-loop-helix (bHLH)",
        "Zinc finger C2H2",
        "P53 like",
        "nuclear receptor ZnC4",
        "homeodomain",
        "winged helix/forkhead",
        "lambda repressor-like",
        "MetR homology",
        "STAT",
        "MADS box",
        "TATA binding protein",
        "HMG box",
        "trp repressor",
        "runt",
        "Zinc-coordinating",
        "SMAD-like",
    ]
    sel_df = dna_df[
        dna_df["nakb_protein_annotations"].isin(selected_annotations)
    ]

    # boxen plots of len by functional annotation, AFTER selection
    color_mapping = get_color_mapping_by_measure(
        sel_df,
        category_column="nakb_protein_annotations",
        measure_column="len_seq",
        palette_name="viridis_r",
    )
    g = sns.catplot(
        data=sel_df,
        x="len_seq",
        y="nakb_protein_annotations",
        kind="boxen",
        height=10,
        aspect=0.8,
        palette=color_mapping,
    )
    g.set(xlabel="Length, measured in base-pairs", ylabel="Protein Annotation")
    # g.fig.subplots_adjust(top=0.9)
#     g.fig.suptitle(
#         "Distributions of Length of DNA Sequences grouped by Functional \
# Annotation Terms (after selection by Annotation)"
#     )
    out_filepath = f"{figures_dir}/annotation-seq_len-boxen-filtered.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(
        f"Boxen plots of sequence lengths by functional annotation, saved at \
{out_filepath}"
    )

    # sequence length distribution AFTER selection
    plt.figure(figsize=(5, 5))
    ax = sns.histplot(
        data=sel_df,
        x="len_seq",
        kde=True,
        # color="#482966",
    )
#     ax.set_title(
#         "Distribution of Length of DNA Sequences Bound by Proteins \
# (after selection by Annotation)"
#     )
    ax.set_xlabel("Length, measured in base-pairs")
    out_filepath = f"{figures_dir}/seq_len-hist-filtered.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(
        f"Histogram of sequence lengths after selections, saved at \
{out_filepath}"
    )

    # by annotation and length
    sel_df = sel_df[(sel_df["len_seq"] >= 5) & (sel_df["len_seq"] < 15)]
    logger.info(
        f"Selection by annotation and length, numerosity: {len(sel_df)}"
    )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
