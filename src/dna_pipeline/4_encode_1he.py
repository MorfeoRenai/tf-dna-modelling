"""Make dataset from DNA sequences bound by interfaces."""

import sys
import logging
from pathlib import Path
import click

import numpy as np
import pandas as pd


def pad_seq(seq, max_len_seq, pad_nucleotide="N"):
    """Add padding to sequence of nucleotides."""
    return seq + pad_nucleotide * (max_len_seq - len(seq))


def encode_dataframe(df, max_len_seq):
    """One hot encoding of nuleotides in sequences."""
    # TODO automatically compute nucleotide set
    nucleotide_set = ["A", "T", "G", "C", "N"]
    dummy_set = pd.get_dummies(nucleotide_set)

    encoded_df = []
    for padded_seq in df["pad_seq"]:
        encoded_seq = np.zeros((max_len_seq, len(nucleotide_set)))
        for i, nucleotide in enumerate(padded_seq):
            encoded_seq[i] = dummy_set[nucleotide].to_numpy()

        # reshape into 1D vector
        encoded_seq = encoded_seq.reshape(-1)

        encoded_df.append(encoded_seq)

    # cast into dataframe
    encoded_df = pd.DataFrame(encoded_df)
    encoded_df.columns = [f"dna-{i}" for i in range(encoded_df.shape[1])]
    logger.info("One-hot-encoding of DNA sequences.")

    return encoded_df


@click.command()
@click.option(
    "--real_dataset_filepath",
    type=click.Path(exists=True),
    help="Bound DNA strings filepath, in .csv format.",
)
@click.option(
    "--counterexample_dataset_filepath",
    type=click.Path(exists=True),
    help="Generated DNA strings filepath, in .csv format.",
)
@click.option(
    "--output_filepath",
    type=click.Path(),
    help="Output filepath, dataset in .csv format.",
)
def main(
    real_dataset_filepath, counterexample_dataset_filepath, output_filepath
):
    """Build dataset of atom coordinates from interfaces."""
    # read csv with interfaces ids and sequences
    real_df = pd.read_csv(real_dataset_filepath, header=0, index_col=0)
    logger.info(f"Loaded dataset {real_dataset_filepath}")

    max_len_seq = max(real_df["len_seq"])
    logger.info(f"Maximum length of DNA sequence: {max_len_seq}")

    # pad at the right of the sequences
    real_df["pad_seq"] = real_df["seq"].apply(pad_seq, max_len_seq=max_len_seq)
    logger.info("Added padding to real DNA sequences.")
    # TODO get reverse complement of sequences

    # read csv with interfaces ids and sequences
    # counterexamples
    ce_df = pd.read_csv(counterexample_dataset_filepath, header=0, index_col=0)
    logger.info(f"Loaded dataset {counterexample_dataset_filepath}")

    # pad at the right of the random sequences
    ce_df["pad_seq"] = ce_df["seq"].apply(pad_seq, max_len_seq=max_len_seq)
    logger.info("Added padding to counterexample DNA sequences.")

    # encode padded sequences that will bind to the interface
    encoded_df = encode_dataframe(real_df, max_len_seq)

    # concatenate DNA info dataframe with encoded DNA sequences dataframe
    # these sequence should be posively predicted
    # concatenate columns
    positive_df = pd.concat([real_df, encoded_df], axis=1)
    positive_df["y"] = 1

    # encode padded sequences that won't bind to the interface
    encoded_df = encode_dataframe(ce_df, max_len_seq)

    # concatenate DNA info dataframe with encoded DNA sequences dataframe
    # these sequence should be negatively predicted
    # concatenate columns
    negative_df = pd.concat([ce_df, encoded_df], axis=1)
    negative_df["y"] = 0

    # final dataframe, concatenate positive and negative dataframes on rows
    final_df = (
        pd.concat([positive_df, negative_df], axis=0)
        .sort_values("interface_id")
        .reset_index(drop=True)
    )

    final_df.to_csv(output_filepath)
    logger.info(
        f"Saved final dataset with both experimental and generated DNA \
sequences, at {output_filepath}"
    )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
