"""Try base pairing on assembly structures."""

import sys
import logging
from pathlib import Path
import click

import pandas as pd
from Bio.PDB.MMCIFParser import MMCIFParser
from PDBNucleic.Structure2DataFrame import get_basepair_dataframe


def trim_basepairs(basepair_df, interacting_df):
    """
    Trim both ends of the basepair dataframe.

    Trimming is based on TF-DNA interactions.
    """
    indexes_to_drop = []

    # trimming from the top
    for idx, row in basepair_df.iterrows():
        i_chain_id = row["i_chain_id"]
        i_residue_index = row["i_residue_index"]
        i_residue_name = row["i_residue_name"]
        j_chain_id = row["j_chain_id"]
        j_residue_index = row["j_residue_index"]
        j_residue_name = row["j_residue_name"]

        # search i residue among the interacting bases
        is_i_base_interacting = (
            interacting_df.loc[
                (interacting_df["chain_id"] == i_chain_id)
                & (interacting_df["res_number"] == i_residue_index)
                & (interacting_df["res_name"] == i_residue_name)
            ]
            # TODO to be uncommeted when using filler for gaps
            # .drop("only_backbone_interactions", axis=1)
            .any().all()
        )

        # search j residue among the interacting bases
        is_j_base_interacting = (
            interacting_df.loc[
                (interacting_df["chain_id"] == j_chain_id)
                & (interacting_df["res_number"] == j_residue_index)
                & (interacting_df["res_name"] == j_residue_name)
            ]
            # TODO to be uncommeted when using filler for gaps
            # .drop("only_backbone_interactions", axis=1)
            .any().all()
        )

        # if both bases are not among the interating bases
        if not is_i_base_interacting and not is_j_base_interacting:
            indexes_to_drop.append(idx)
        else:
            break

    # trimming from the bottom
    for idx, row in basepair_df[::-1].iterrows():
        i_chain_id = row["i_chain_id"]
        i_residue_index = row["i_residue_index"]
        i_residue_name = row["i_residue_name"]
        j_chain_id = row["j_chain_id"]
        j_residue_index = row["j_residue_index"]
        j_residue_name = row["j_residue_name"]

        # search i residue among the interacting bases
        is_i_base_interacting = (
            interacting_df.loc[
                (interacting_df["chain_id"] == i_chain_id)
                & (interacting_df["res_number"] == i_residue_index)
                & (interacting_df["res_name"] == i_residue_name)
            ]
            # TODO to be uncommeted when using filler for gaps
            # .drop("only_backbone_interactions", axis=1)
            .any().all()
        )

        # search j residue among the interacting bases
        is_j_base_interacting = (
            interacting_df.loc[
                (interacting_df["chain_id"] == j_chain_id)
                & (interacting_df["res_number"] == j_residue_index)
                & (interacting_df["res_name"] == j_residue_name)
            ]
            # TODO to be uncommeted when using filler for gaps
            # .drop("only_backbone_interactions", axis=1)
            .any().all()
        )

        # if both bases are not among the interating bases
        if not is_i_base_interacting and not is_j_base_interacting:
            indexes_to_drop.append(idx)
        else:
            break

    # trimming
    basepair_df = basepair_df.drop(indexes_to_drop, axis=0)

    return basepair_df


@click.command()
@click.option(
    "--assembly_dir",
    type=click.Path(exists=True),
    help="Directory where to copy the assemblies.",
)
@click.option(
    "--interacting_dataset_filepath",
    type=click.Path(exists=True),
    help="Filepath to dataset with interacting bases.",
)
@click.option(
    "--output_filepath",
    type=click.Path(),
    help="Filepath to output DNA sequences bound by TFs, in .csv format.",
)
def main(assembly_dir, interacting_dataset_filepath, output_filepath):
    """Make dataset with DNA sequences bound by TFs."""
    interacting_df = pd.read_csv(interacting_dataset_filepath)

    # list of tuples, to be casted into output dataframe
    valid_data = []

    # list of tuples, to be casted into output dataframe
    # for interfaces that returned errors
    invalid_data = []

    parser = MMCIFParser(QUIET=True)

    grouping = interacting_df.groupby("interface_id")
    for interface_id, grouped_interacting_df in grouping:
        # descriptiona and annotation for every interface
        description = grouped_interacting_df["description"].iloc[0]
        nakb_protein_annotations = grouped_interacting_df[
            "nakb_protein_annotations"
        ].iloc[0]

        # pdb_id and assembly filepath
        pdb_id = interface_id[:4]
        assembly_filepath = f"{assembly_dir}/{pdb_id.lower()}-assembly1.cif"

        # parse structure from mmCif file
        structure = parser.get_structure(pdb_id, assembly_filepath)

        # polymer_df = polymer_dataframe_from_structure(structure)
        # nucleic_chain_ids = polymer_df[
        #     polymer_df["polymer_type"] == "polymer_nucleic"
        # ]["chain_id"].tolist()
        # if len(nucleic_chain_ids) % 2 != 0:
        #     # print(pdb_id, nucleic_chain_ids)
        #     continue

        # TODO understand why it return errors sometimes
        try:
            basepair_df = get_basepair_dataframe(structure)
        except KeyError as e:
            logger.error(f"KeyError {e} at {assembly_filepath}")

            # add to invalid data, to be exported
            invalid_data.append((interface_id, assembly_filepath, f"{e}"))
            continue

        # TODO add processing AKA filling gaps
        # basepair_df = process_paired_bases(basepair_df, structure)
        basepair_df = trim_basepairs(basepair_df, grouped_interacting_df)

        # check if there is only one interrupted bp segment
        segments = basepair_df["paired_segment"].unique().size
        if segments == 1:
            # build a list of nucleotides, from one strand only
            nucleotides = [res for res in basepair_df["i_residue_name"]]

            # build a string of nucleotides
            seq = " ".join(nucleotides)

            # compute length of nucleotide sequence
            len_seq = len(nucleotides)

            logger.info(
                f"{interface_id} has one segment bound. Sequence of length \
{len_seq}"
            )
            # add to data, to be exported
            valid_data.append(
                (
                    interface_id,
                    description,
                    nakb_protein_annotations,
                    seq,
                    len_seq,
                )
            )
        else:
            logger.warning(
                f"{interface_id} has more than one segment bound: {segments}"
            )
            # add to invalid data, to be exported
            invalid_data.append(
                (interface_id, assembly_filepath, f"segments: {segments}")
            )

    logger.info("Saving valid interfaces ...")
    pd.DataFrame(
        valid_data,
        columns=[
            "interface_id",
            "description",
            "nakb_protein_annotations",
            "seq",
            "len_seq",
        ],
    ).to_csv(output_filepath)

    logger.info("Saving not valid interfaces ...")
    pd.DataFrame(
        invalid_data, columns=["interface_id", "filepath", "notes"]
    ).to_csv("data/interim/invalid_interfaces.csv")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
