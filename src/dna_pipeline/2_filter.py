"""Try base pairing on assembly structures."""

import sys
import logging
from pathlib import Path
import click

import pandas as pd


def custom_filter(df):
    """Filter basepairs dataframe."""
    # selected annotations
    selected_annotations = [
        "rel homology",
        "leucine zipper (bZIP)",
        "helix-loop-helix (bHLH)",
        "Zinc finger C2H2",
        "P53 like",
        "nuclear receptor ZnC4",
        "homeodomain",
        "winged helix/forkhead",
        "lambda repressor-like",
        "MetR homology",
        "STAT",
        "MADS box",
        "TATA binding protein",
        "HMG box",
        "trp repressor",
        "runt",
        "Zinc-coordinating",
        "SMAD-like",
    ]

    # filter by annotation
    df = df[df["nakb_protein_annotations"].isin(selected_annotations)]

    # filter by length
    df = df[(df["len_seq"] >= 5) & (df["len_seq"] < 15)]

    # reset index
    df = df.reset_index(drop=True)

    return df


@click.command()
@click.option(
    "--input_filepath",
    type=click.Path(exists=True),
    help="Filepath to dataset with interim dataset, in .csv format.",
)
@click.option(
    "--output_filepath",
    type=click.Path(),
    help="Filepath to output DNA sequences bound by TFs, in .csv format.",
)
def main(input_filepath, output_filepath):
    """Filter and process DNa dataset."""
    # read csv with interfaces ids and sequences
    df = pd.read_csv(input_filepath, header=0, index_col=0)
    logger.info(f"Loaded dataset {input_filepath}")

    # filtering
    df = custom_filter(df)
    logger.info(
        "Filtered dataset using functional annotations and sequence lengths."
    )

    # flag for real vs counterexamples
    df["is_real"] = True

    # 1 letter nucleotides
    df["seq"] = df["seq"].apply(
        lambda seq: "".join([i[1] for i in seq.split(" ")])
    )
    logger.info("Convert from 2-letter to 1-letter nucleotides.")

    df.to_csv(output_filepath)
    logger.info(
        f"Saved filtered dataset with both experimental and generated DNA \
sequences, at {output_filepath}"
    )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
