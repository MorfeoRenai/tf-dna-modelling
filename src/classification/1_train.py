"""Classification: predicting interface-DNA binding."""

import sys
import logging
from pathlib import Path
import click

import pickle
import pandas as pd

from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier

# from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier

# local imports
from utils import mkdir_new_model
from utils import save_hyperparams


def train_model(X, y, model, model_name, bins_model_dir, param_grid) -> None:
    """Train and validate given model."""
    # make new directory for model
    model_dir = mkdir_new_model(model_name, models_dir=bins_model_dir)

    # hyperparameters tuning and cross-validation
    grid_search = GridSearchCV(
        model,
        param_grid=param_grid,
        scoring="roc_auc",
        cv=5,
        n_jobs=8,
        verbose=4,
    )
    grid_search.fit(X, y)

    # save best model from the hyperparameters tuning and cross-validation
    model = grid_search.best_estimator_
    with open(f"{model_dir}/model_train.pkl", "wb") as f:
        pickle.dump(model, f)

    # save best combination of hyperparameters in the grid
    best_hyperparams = grid_search.best_params_
    save_hyperparams(model_dir, dict_hyperparams=best_hyperparams)

    # save cross-validation results
    cv_results = pd.DataFrame(grid_search.cv_results_)
    cv_results.to_csv(f"{model_dir}/summary.csv")


@click.command()
@click.option(
    "--dataset_filepath",
    type=click.Path(exists=True),
    help="Filepath to dataset for classifers, in .csv format.",
)
@click.option(
    "--classifiers_dir",
    type=click.Path(exists=True),
    help="Directory with regressor models, summaries etc.",
)
def main(dataset_filepath, classifiers_dir):
    """Classification: predicting interface-DNA binding."""
    df = pd.read_csv(dataset_filepath, header=0, index_col=0)

    for bins in df["bins"].unique():
        # mkdir for binning value
        bins_model_dir = Path(f"{classifiers_dir}/bins-{bins}/")
        bins_model_dir.mkdir(parents=True, exist_ok=True)

        # select X based on binnings for interface fingerprints
        X = df[df["bins"] == bins]
        X = X.drop("bins", axis=1)

        # select only features from DNA and interfaces
        X = X.loc[:, "dna-0":]

        # drop all columns with all NA, so the columns used with bigger bins
        X = X.dropna(axis=1, how="all")

        # predictor feature
        y = X.pop("y")

        # K nearest neighbors classifier
        param_grid = {
            "n_neighbors": [3, 5, 10, 25, 50, 100],
        }
        logger.info(f"Training KNN Classifier model with bins={bins} ...")
        train_model(
            X=X,
            y=y,
            model=KNeighborsClassifier(),
            param_grid=param_grid,
            model_name="knn",
            bins_model_dir=bins_model_dir,
        )
        logger.info(
            f"Finished training KNN Classifier model with bins={bins}."
        )

        #         # support vector machine classifer
        #         param_grid = {
        #             "C": [0.001, 0.1, 1, 10, 100, 1000],
        #             "kernel": ["rbf", "linear", "poly"],
        #             "gamma": [10, 1, 0.1, 0.01, 0.001, 0.0001],
        #         }
        #         logger.info(
        #             f"Training Support Vector Classifier model with \
        # bins={bins} ..."
        #         )
        #         train_model(
        #             X=X,
        #             y=y,
        #             model=SVC(),
        #             param_grid=param_grid,
        #             model_name="svc",
        #             bins_model_dir=bins_model_dir,
        #         )
        #         logger.info(
        #             f"Finished training Support Vector Classifier model \
        # with bins={bins}."
        #         )

        # random forest classifier
        param_grid = {
            "n_estimators": [25, 50, 100, 150],
            "max_features": ["sqrt", "log2", None],
            "max_depth": [3, 6, 9],
            "max_leaf_nodes": [3, 6, 9],
        }
        logger.info(
            f"Training Random Forest Classifier model with bins={bins} ..."
        )
        train_model(
            X=X,
            y=y,
            model=RandomForestClassifier(),
            param_grid=param_grid,
            model_name="randomforest",
            bins_model_dir=bins_model_dir,
        )
        logger.info(
            f"Finished training Random Forest Classifier model with bins=\
{bins}."
        )

        # random forest classifier
        param_grid = {
            "n_estimators": [50, 100, 200],
            "learning_rate": [0.01, 0.1, 0.2],
            "max_depth": [3, 5, 7],
        }
        logger.info(
            f"Training Gradient Boosting Classifier model with bins={bins} ..."
        )
        train_model(
            X=X,
            y=y,
            model=GradientBoostingClassifier(),
            param_grid=param_grid,
            model_name="gboost",
            bins_model_dir=bins_model_dir,
        )
        logger.info(
            f"Finished training Gradient Boosting Classifier model with bins=\
{bins}."
        )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
