"""Visualize classifier models."""

import sys
import logging
from pathlib import Path
import click

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pickle

from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay


@click.command()
@click.option(
    "--dataset_filepath",
    type=click.Path(exists=True),
    help="Filepath to dataset for classifers, in .csv format.",
)
@click.option(
    "--classifiers_dir",
    type=click.Path(exists=True),
    help="Directory with classifiers models, summaries etc.",
)
@click.option(
    "--figures_dir",
    type=click.Path(exists=True),
    help="Output directory with figures.",
)
def main(dataset_filepath, classifiers_dir, figures_dir):
    """Visualize classifier models."""
    # meta-summary: summary of summaries
    meta_summary = []

    for filepath in Path(classifiers_dir).iterdir():
        if not (filepath.is_dir() and "bins" in filepath.name):
            continue
        _, bins = filepath.name.split("-")
        bins = int(bins)
        for filepath2 in filepath.iterdir():
            if not filepath.is_dir():
                continue
            model_name, model_num = filepath2.name.split("-")
            model_num = int(model_num)
            summary_filepath = filepath2 / "summary.csv"
            df = pd.read_csv(summary_filepath, header=0, index_col=0)

            # take the best model
            df = df[df["rank_test_score"] == 1]

            # take the cross validated score (ROC AUC in this case)
            # by slicing until "mean_test_score" it takes a variable number
            # of splits/cv values, instead of hard-coding it
            df = df.loc[:, "split0_test_score":"mean_test_score"]
            df = df.drop("mean_test_score", axis=1)

            for roc_auc in df.values.reshape(-1):
                row = (bins, model_name, model_num, roc_auc)
                meta_summary.append(row)

    # meta-summary: summary of summaries
    meta_summary = pd.DataFrame(
        meta_summary, columns=["bins", "model_name", "model_num", "roc_auc"]
    ).sort_values(by="bins")

    # box plots
    g = sns.catplot(
        data=meta_summary,
        y="roc_auc",
        x="bins",
        hue="model_name",
        kind="box",
        height=5,
        aspect=1,
    )
    g.set(
        xlabel="Bin number for interface fingerprint",
        ylabel="Cross-validated ROC AUC",
    )
    # g.fig.subplots_adjust(top=0.9)
    # g.fig.suptitle(
    #     "Cross-Validated ROC AUC for Every Classifier-Binning Permutation"
    # )
    out_filepath = f"{figures_dir}/cv_roc_auc-box.pdf"
    plt.savefig(out_filepath, format="pdf")
    logger.info(
        f"Boxplots of cross-validated ROC AUC for every classifier at \
{out_filepath}"
    )

    # choose one bin-model permutation as best model overall
    bins = 4
    with open(f"{classifiers_dir}/bins-{bins}/gboost-0/model_train.pkl",
              "rb") as f:
        model = pickle.load(f)

    df = pd.read_csv(dataset_filepath, header=0, index_col=0)

    # select only features from DNA and interfaces
    X = df.loc[:, "dna-0":"y"]

    # select X based on binnings for interface fingerprints
    X = X[X["bins"] == bins]
    X = X.drop("bins", axis=1)

    # predictor feature
    y = X.pop("y")

    # drop all columns with all NA, so the columns used with bigger bins
    X = X.dropna(axis=1, how="all")

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42)

    # refitting
    model.fit(X_train, y_train)

    # Predict probabilities on the train set
    y_pred_proba = model.predict_proba(X_train)[:, 1]

    # histogram probabilities
    plt.figure()
    g = sns.histplot(
        data=y_pred_proba,
        stat="frequency",
        bins=50,
        kde=True,
    )
    out_filepath = f"{figures_dir}/probabilities-hist-train.pdf"
    plt.savefig(out_filepath, format="pdf")

    # Predict probabilities on the test set
    y_pred_proba = model.predict_proba(X_test)[:, 1]

    # Calculate ROC curve
    fpr, tpr, thresholds = roc_curve(y_test, y_pred_proba)
    roc_auc = auc(fpr, tpr)
    # Plot the ROC curve
    plt.figure(figsize=(5, 5))
    plt.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], 'k--', label='No Skill')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend()
    out_filepath = f"{figures_dir}/roc_curve.pdf"
    plt.savefig(out_filepath, format="pdf")

    # confusion matrix on train set
    y_pred = model.predict(X_train)
    cm = confusion_matrix(y_train, y_pred, labels=model.classes_)
    disp = ConfusionMatrixDisplay(confusion_matrix=cm,
                                  display_labels=model.classes_)
    plt.figure(figsize=(5, 5))
    disp.plot()
    out_filepath = f"{figures_dir}/confusion_matrix-train.pdf"
    plt.savefig(out_filepath, format="pdf")

    # confusion matrix on test set
    y_pred = model.predict(X_test)
    cm = confusion_matrix(y_test, y_pred, labels=model.classes_)
    disp = ConfusionMatrixDisplay(confusion_matrix=cm,
                                  display_labels=model.classes_)
    plt.figure(figsize=(5, 5))
    disp.plot()
    out_filepath = f"{figures_dir}/confusion_matrix-test.pdf"
    plt.savefig(out_filepath, format="pdf")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
