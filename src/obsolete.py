import logging

from PDBNucleic.utils import get_paired_segments


# def process_interacting_bases(interacting_df):
#     """
#     Process.

#     Consist in getting unique bases, plus a bool column, indicating if the
#     is interacted with only through its backbone (sugare, phosphate)
#     """
#     # grouping by base
#     grouping = interacting_df.groupby(
#         by=["chain_id", "res_number", "res_name"]
#     )["dna_moiety"]

#     # grouping result is one single column/Series
#     is_backbone_column = grouping.apply(
#         lambda dna_moiety_col: all(  # given the interactions with a base ...
#             map(  # ... check if all these interactions
#                 # ... are interactions with the DNA backbone
#                 # (phosphate and sugar)
#                 lambda x: x in ["pp", "sr"],
#                 dna_moiety_col,
#             )
#         )
#     )

#     # renaming Series
#     is_backbone_column.name = "only_backbone_interactions"

#     # from multiindex series to dataframe
#     interacting_df = is_backbone_column.reset_index()

#     return interacting_df


def process_paired_bases(basepair_df, structure):
    gap_to_fill = 1

    result = basepair_df.copy()

    previous_i_chain_id = None
    previous_j_chain_id = None
    previous_i_residue_index = None
    previous_j_residue_index = None
    for idx, row in basepair_df.iterrows():
        current_i_chain_id = row["i_chain_id"]
        current_j_chain_id = row["j_chain_id"]
        current_i_residue_index = row["i_residue_index"]
        current_j_residue_index = row["j_residue_index"]

        if previous_i_chain_id is not None:
            # step case

            is_i_strand_gapped = (
                abs(previous_i_residue_index - current_i_residue_index)
                == gap_to_fill + 1
            )
            is_j_strand_gapped = (
                abs(previous_j_residue_index - current_j_residue_index)
                == gap_to_fill + 1
            )

            # check for a gap in both i and j strands
            if (
                previous_i_chain_id == current_i_chain_id
                and previous_j_chain_id == current_j_chain_id
                and is_i_strand_gapped
                and is_j_strand_gapped
            ):

                # TODO
                try:
                    i_resname = structure[0][previous_i_chain_id][
                        previous_i_residue_index + 1
                    ].resname
                    j_resname = structure[0][previous_j_chain_id][
                        previous_j_residue_index - 1
                    ].resname
                except KeyError as e:
                    logger.warning(
                        f"{e}: can't get residue in chain \
{structure[0][previous_i_chain_id].full_id}, probably is a hetero-residue."
                    )
                    # heteroatoms
                    # some PDBs have no concept of ZERO in the res number
                    # for some reason!
                    if previous_i_residue_index == -1:
                        previous_i_residue_index = 0
                    if previous_j_residue_index == 1:
                        previous_j_residue_index = 0

                    i_resname_list = [
                        residue.resname
                        for residue in structure[0][previous_i_chain_id]
                        if residue.id[1] == previous_i_residue_index + 1
                    ]
                    j_resname_list = [
                        residue.resname
                        for residue in structure[0][previous_j_chain_id]
                        if residue.id[1] == previous_j_residue_index - 1
                    ]

                    if len(i_resname_list) and len(j_resname_list):
                        i_resname = i_resname_list[0]
                        j_resname = j_resname_list[0]
                    else:
                        logger.warning(
                            f"Can't find residue in chain \
{structure[0][previous_i_chain_id].full_id}, very elusive. \
It will annotated with 'X'"
                        )
                        i_resname = "X"
                        j_resname = "X"

                row = (
                    previous_i_chain_id,
                    previous_i_residue_index + 1,
                    i_resname,
                    j_resname,
                    previous_j_residue_index - 1,
                    previous_j_chain_id,
                    None,
                )

                result.loc[idx - 0.5] = row

        previous_i_chain_id = current_i_chain_id
        previous_j_chain_id = current_j_chain_id
        previous_i_residue_index = current_i_residue_index
        previous_j_residue_index = current_j_residue_index

    result = result.sort_index().reset_index(drop=True)

    result["paired_segment"] = get_paired_segments(result)

    return result


logger = logging.getLogger(__name__)
