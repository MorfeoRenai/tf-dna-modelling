"""Make requests to database."""

import sys
import logging
from pathlib import Path
import click

import shutil
import sqlite3
import pandas as pd


def query_interacting_bases(db_filepath, interacting_dataset_filepath) -> None:
    """Make requests for interacting bases data."""
    # Creare una connessione al database SQLite
    conn = sqlite3.connect(db_filepath)

    # Creare un cursore
    cur = conn.cursor()

    # Eseguire una query
    cur.execute(
        """
        SELECT Interactions.interface_id, Polymers.nakb_protein_annotations,
        Polymers.description,
        chain_id, res_number, res_name, dna_moiety
        FROM Interactions
        INNER JOIN Atoms
        ON Interactions.dna_atom_id = Atoms.atom_id
        INNER JOIN Interfaces
        ON Interactions.interface_id = Interfaces.interface_id
        INNER JOIN Polymers
        ON Interfaces.entity_id = Polymers.entity_id
        """
    )

    # Risultati della query sotto forma di DataFrame
    data = cur.fetchall()
    df = pd.DataFrame(
        data,
        columns=[
            "interface_id",
            "nakb_protein_annotations",
            "description",
            "chain_id",
            "res_number",
            "res_name",
            "dna_moiety",
        ],
    )

    # Chiudi la connessione
    conn.commit()
    conn.close()

    df.to_csv(interacting_dataset_filepath)
    logger.info(f"Exported queried interacting bases into \
{interacting_dataset_filepath}")
    # return df


def copy_interface_files(source_dir, destination_dir):
    """Make requests for interfaces files."""
    # TODO proper HTTP request

    # Source directory path
    source_dir = Path(source_dir)
    # Destination directory path
    destination_dir = Path(destination_dir)

    # Create the destination directory if it doesn't exist
    destination_dir.mkdir(parents=True, exist_ok=True)

    # Iterate through all files in the source directory
    for source_file in source_dir.iterdir():
        if (
            not source_file.is_file()  # Check if it's a file before copying
            or "interface.cif"
            not in source_file.name  # an check it's the correct file
        ):
            continue

        try:
            # Set the output path for the structure file
            destination_file = destination_dir / source_file.name

            # Copy file
            shutil.copy(source_file.as_posix(), destination_file.as_posix())

            logger.info(f"Copied {source_file.name}")
        except Exception as e:
            logger.warning(f"Failed to copy {source_file.name}: {e}")


def copy_assembly_files(source_dir, destination_dir):
    """Make requests for assembly files."""
    # TODO proper HTTP request

    # Source directory path
    source_dir = Path(source_dir)
    # Destination directory path
    destination_dir = Path(destination_dir)

    # Create the destination directory if it doesn't exist
    destination_dir.mkdir(parents=True, exist_ok=True)

    # Iterate through all files in the source directory
    for source_file in source_dir.iterdir():
        if (
            not source_file.is_file()  # Check if it's a file before copying
            or "assembly"
            not in source_file.name  # an check it's the correct file
        ):
            continue

        try:
            # Set the output path for the structure file
            destination_file = destination_dir / source_file.name

            # Copy file
            shutil.copy(source_file.as_posix(), destination_file.as_posix())

            logger.info(f"Copied {source_file.name}")
        except Exception as e:
            logger.warning(f"Failed to copy {source_file.name}: {e}")


@click.command()
@click.option(
    "--db_filepath",
    type=click.Path(exists=True),
    help="Database from where to query data.",
)
@click.option(
    "--source_dir",
    type=click.Path(exists=True),
    help="Directory with all the structures, from database.",
)
@click.option(
    "--assembly_dir",
    type=click.Path(),
    help="Directory where to copy the assemblies.",
)
@click.option(
    "--interface_dir",
    type=click.Path(),
    help="Directory where to copy the interfaces.",
)
@click.option(
    "--interacting_dataset_filepath",
    type=click.Path(),
    help="Filepath to output dataset with interacting bases.",
)
def main(
    db_filepath,
    source_dir,
    assembly_dir,
    interface_dir,
    interacting_dataset_filepath,
):
    """Request data from database."""
    copy_assembly_files(source_dir, assembly_dir)
    copy_interface_files(source_dir, interface_dir)
    query_interacting_bases(db_filepath, interacting_dataset_filepath)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(stream=sys.stdout, format=log_fmt, level=logging.INFO)
    logger = logging.getLogger(__name__)

    # not used in this script but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    main()
