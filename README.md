TF-DNA Modelling
================

A thesis project about modelling Transcription Factors - DNA interactions through neural networks.

![Graphical Abstract](reports/figures/graphical_abstract.svg "Graphical Abstract")


## Project Organization

    ├── .gitignore
    ├── .gitlab-ci.yml     <- GitLab Continous Integration config file.
    ├── LICENSE            <- GPL-3 license.
    ├── Makefile           <- Makefile with commands like `make data` or `make train`.
    ├── README.md          <- The top-level README for developers using this project.
    ├── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io
    │
    ├── data
    │   ├── raw            <- Data requested from database, not to be touched.
    │   │   ├── assemblies <- TF-DNA complexes biological assemblies in mmCif format.
    │   │   │
    │   │   └── interfaces <- TF interfaces in mmCif format.
    │   │
    │   ├── interim        <- Data being wrangled and explored, ready for visualization.
    │   │
    │   └── processed      <- The final, canonical datasets for modeling.
    │
    ├── envs               <- YAML files for building `conda` environments for every analysis step.
    │
    ├── logs               <- Logs of every step of the analysis.
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries.
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-ap-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting.
    │
    └── src                <- Source code for use in this project.
        ├── __init__.py    <- Makes src a Python module.
        │
        ├── data           <- Scripts to download or generate data.
        │
        ├── dna_pipeline   <- Scripts to process DNA sequences.
        │
        ├── interface_pipeline <- Scripts to process TF interfaces through Graph Neural Networks.
        │
        ├── classification <- Scripts to train and visualize classifiers.
        │
        └── regression     <- Scripts to train and visualize regressors.


## Dependencies

This project uses `conda` virtual environments and `make` for managing dependencies for every analysis step.

I recommend using `mamba` or `micromamba` (we went with this), which are reimplementations
of the `conda` package manager with much faster dependency solving and parallel downloading.

Using `micromamba` version 1.5.8 will require the channel priority to be set to flexible. Probably the same will be necessary with `conda` and `mamba`, just be aware.

    $ conda config --set channel_priority flexible
    $ mamba config set channel_priority flexible
    $ micromamba config set channel_priority flexible


## Workflow

The whole workflow is summarized by the [graphical abstract](reports/figures/graphical_abstract.svg) and is composed of the following steps:

1. Set up a `conda` virtual environments and install dependencies;
2. Request data from database;
3. Parallel pipelines:
    - DNA sequences processing:
        1. Build interim dataset of DNA sequences;
        2. Explore DNA sequences;
        3. Filter DNA sequences;
        4. Generate counter examples from DNA sequences;
        5. Encode numerically;
    - Interface processing:
        1. Explore interfaces;
        2. Build dataset for autoencoder model;
        3. Train autoencoder model;
        4. Predict with autoencoder model;
        5. Plot predictions of autoencoder model;
        6. Build molecular fingerprints from latent vectors;
4. Classification:
    1. Build dataset for classification model, based on both DNA encoding and interface fingerprint;
    2. Train classifier models;
    3. Plot predictions of classifier models;
5. Regression:
    1. Build dataset for regression model, based on both DNA encoding and interface fingerprint;
    2. Train regression models;
    3. Plot predictions of regression models;


<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
